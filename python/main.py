import json
import socket
import sys
import math
import numpy as np
###FIXME
#from random import randint
#from datetime import datetime

class PID:
    """
    Discrete PID control
    """

    def __init__(self, P=2.0, I=0.0, D=1.0, Derivator=0, Integrator=0, Integrator_max=500, Integrator_min=-500):

        self.Kp=P
        self.Ki=I
        self.Kd=D
        self.Derivator=Derivator
        self.Integrator=Integrator
        self.Integrator_max=Integrator_max
        self.Integrator_min=Integrator_min

        self.UseFF = False
        self.FeedForward=0.0

        self.set_point=0.0
        self.error=0.0

    def update(self,current_value):
        """
        Calculate PID output value for given reference input and feedback
        """

        self.error = self.set_point - current_value

        self.P_value = self.Kp * self.error
        self.D_value = self.Kd * ( self.error - self.Derivator)
        self.Derivator = self.error

        self.Integrator = self.Integrator + self.error

        if self.Integrator > self.Integrator_max:
            self.Integrator = self.Integrator_max
        elif self.Integrator < self.Integrator_min:
            self.Integrator = self.Integrator_min

        self.I_value = self.Integrator * self.Ki

        if self.UseFF:
            PID = self.P_value + self.I_value + self.D_value + self.FeedForward
        else:
            PID = self.P_value + self.I_value + self.D_value

        return PID

    def setPoint(self,set_point, zero=False):
        """
        Initilize the setpoint of PID
        """
        self.set_point = set_point
        if zero:
            self.Integrator=0
            self.Derivator=0

    def setIntegrator(self, Integrator):
        self.Integrator = Integrator

    def setDerivator(self, Derivator):
        self.Derivator = Derivator

    def setKp(self,P):
        self.Kp=P

    def setKi(self,I):
        self.Ki=I

    def setKd(self,D):
        self.Kd=D

    def setFeedForward(self,FF, useFF=True):
        self.FeedForward = FF
        self.UseFF = useFF

    def getPoint(self):
        return self.set_point

    def getError(self):
        return self.error

    def getIntegrator(self):
        return self.Integrator

    def getDerivator(self):
        return self.Derivator

class SpeedRacer(object):

    def __init__(self, socket, name, key):
        # ASCII http://patorjk.com/software/taag/#p=display&f=Colossal&t=print%20table
        # for testing
        self.DEBUG = False
        self.BOOST = True

        # Game and Racer variables
        self.socket = socket
        self.name = name
        self.key = key

        # available tracks
        self.availableTracks = {
            'ke': 'keimola',
            'ge': 'germany',
            'us': 'usa',
            'fr': 'france',
            'el': 'elaeintarha',
            'im': 'imola',
            'en': 'england',
            'su': 'suzuka',
            'pe': 'pentag'
        }
        self.trackTimes = {
            'ke': 6080,
            'ge': 8200,
            'us': 4030,
            'fr': 6050,
            'el': 4900,
            'im': 6900,
            'en': 8400,
            'su': 8100,
            'pe': 8300
        }

        # store switch lengths by abs(angle) then by start length and end length
        self.switchLengths = {
            0: {
                70.0: {70.0: 72.90457277751446},
                78.0: {78.0: 80.61901356381203},
                90.0: {90.0: 92.2816812724387},
                94.0: {94.0: 96.1876570838152},
                99.0: {99.0: 101.0804661468},
                100.0: {100.0: 102.06027499293377},
                102.0: {102.0: 104.02104618102567},
                110: {110.0: 111.87803306256944}
            },
            22.5: {
                78.53981633974483: {86.39379797371932: 84.86578339297539, 70.68583470577035: 77.25443789318354},
                86.39379797371932: {78.53981633974483: 84.86503311033147},
                70.68583470577035: {78.53981633974483: 77.25518336641706},
                74.61282552275759: {82.46680715673207: 81.05465220637494},
                82.46680715673207: {74.61282552275759: 81.05390415930518}
            },
            45: {
                47.12388980384689: {62.83185307179586: 58.50306675222316},
                62.83185307179586: {47.12388980384689: 58.50169320233742, 78.53981633974483: 73.44826245632035},
                94.24777960769379: {78.53981633974483: 88.6567400124352},
                70.68583470577035: {86.39379797371932: 81.02948414200813},
                78.53981633974483: {94.24777960769379: 88.65817365363483, 62.83185307179586: 73.44684978162711},
                86.39379797371932: {70.68583470577035: 81.02805951671859}
            },
            80: {
                41.88790204786391: {69.81317007977319: 59.13998341937874},
                69.81317007977319: {41.88790204786391: 59.13782775482745, 97.73843811168244: 85.83496838237477},
                97.73843811168244: {69.81317007977319: 85.8327075862191}
            },
            90.0: {
                54.97787143782138: {86.39379797371932: 73.06207565942928},
                86.39379797371932: {54.97787143782138: 73.05966991777574, 117.80972450961724: 103.47743016894084},
                117.80972450961724: {86.39379797371932: 103.47495078583074}
            },
            60.0: {
                94.2477796076938: {73.30382858376184: 86.04113297510435},
                73.30382858376184: {94.2477796076938: 86.04295549224629, 52.35987755982988: 65.88050610571158},
                52.35987755982988: {73.30382858376184: 65.88228042574654}
            }
        }



        
        """{
            0: {
                110: {110.0: 111.87803306256944},
                99.0: {99.0: 101.0804661468},
                100.0: {100.0: 102.06027499293377},
                70: {70.0: 72.90457277751446},
                94.0: {94.0: 96.1876570838152}
            },
            22.5: {
                210: {190: 81.05390415930475}, 
                190: {210: 81.05465220637494}
            }, 
            45: {100: {80: 73.44684978162711, 120: 88.65817365363483}, 
                110: {90: 81.02805951671859}, 
                80: {60: 58.501693202337414, 100: 73.44826245632036}, 
                120: {100: 88.6567400124352}, 
                90: {110: 81.02948414200824}, 
                60: {80: 58.50306675222316}
            },
            80: {}
        }"""

        # command que
        # GAS, LANE, TURBO
        self.commandStack = []

        # PID variables and setup
        self.pid = PID(15.0,0.4,3.0)
        self.pid.setPoint(1.0)
        self.pid.setFeedForward(1.0, useFF=True)
        self.pidactive = True

        # transfer function and other car constants
        self.timeConstant = 0
        self.maxSpeed = 0
        self.Fslip = 0.35 # Initial value, we change this later
        self.zza = 0.1 # the angle that we will compare the new zero-zero-angle against
        self.FslipFound = False
        self.mass = 0
        self.v1 = 0
        self.v2 = 0
        self.k = 0
        self.k1 = 0
        self.k2 = 0

        self.bumpTick = 0
        self.initialMultiplier = 1.0
        # racer details
        self.cars = {}
        # position of racers
        self.positions = {}

        # game details
        self.gametick = -1
        self.gametickSent = -2
        self.lap = 0
        self.lane = "Left"
        self.track = []
        self.totalPieces = 0
        self.laneChanges = {}
        self.racing = True
        self.totalLaps = 0
        self.qualifydurationMs = 0

        # changing every tick details
        self.theGas = 1.0
        self.theLastGas = 1.0
        self.testGas = 1.0
        self.pPos = 0
        self.pTile = 0
        self.pAngle = 0
        self.pAngleRate = 0
        self.pAngleRateRate = 0
        self.pLane = -1
        self.cPos = 0
        self.cTile = 0
        self.nTile = 1
        self.cAngle = 0
        self.cAngleRate = 0
        self.cAngleRateRate = 0
        self.clane = -1
        self.cLapDistance = 0
        self.v = 0
        self.a = 0

        self.magicA = 0.53033008589
        self.magicAfound = False

        self.fastestLap = 99999

        # for angle slip constants
        # alpha + W*w + T*theta*v = +/- (A*^2/ssqrt(R) - B*v)
        self.slipFound = False
        self.W = 0
        self.T = 0
        self.A = 0
        self.B = 0
        self.step1 = [0,0,0,0]
        self.step2 = [0,0,0,0]
        self.step3 = [0,0,0,0]
        self.step4 = [0,0,0,0]
        self.b = [0,0,0,0]
        # in use
        self.atemp = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
        self.btemp = [0,0,0,0]
        self.rtemp = [0,0,0,0] # radius and sign from direction
        self.slipCheckTick = 0
        # ALL
        self.slips = {} # dictionary containing slip data for each corner
        self.atempall = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
        self.btempall = [0,0,0,0]
        self.rtempall = [0,0,0,0] # radius and sign from direction
        self.slipCheckTickall = 0

        # for increasing speed through corners
        self.turndirection = ""
        self.turnMaxTheta = 0
        self.turnStart = 0
        self.turnFullThrottle = False

        self.braketick = -1
        self.thetatripledot = 0
        self.testskills = False
        # check these
    #    self.states = {"Brake": 0, "Accelerate": 1, "Turn Brake": 2, "Turn Accelerate": 4, "Switch": 8}
    #    self.state = 1

        #turbo
        self.turboAvailable = False
        self.turboData = {}
        self.turboTile = 999
        self.turboState = False
        self.turboStraight = 999

        self.homeStraightTile = 999


        #for overtaking
###DATALOG        self.Follow = False
        self.following = False
        self.whotofollow = ""
        self.didLaneChange = False
        self.aheadx = 0
        self.letsBumpem = False
        self.BumperChangedAt = -1

        # for the people
        self.vmax = 0
        self.amax = 0
###DATALOG
        self.anglemax = 0
        self.anglemin = 0

###DATALOG
        self.debugMessage = "go fast or go home"
        self.debugdistance = 0
        self.dip = PID(15.0,0.4,3.0)
        self.dip.setPoint(9.5)
        self.dip.setFeedForward(9.5, useFF=True)


    def msg(self, msg_type, data):
        #if self.gametick > 590:
        #print "sending", msg_type, self.gametick, data
        #if self.gametick == self.gametickSent:
        #    print "already sent this gametick"
        #else:
        if msg_type == 'ping' or msg_type == 'turbo' or msg_type == 'join' or msg_type == 'createRace' or msg_type == 'joinRace' or self.gametick == -1:
            self.send(json.dumps({"msgType": msg_type, "data": data}))
        else:
            self.send(json.dumps({"msgType": msg_type, "data": data, "gametick": self.gametick}))
        #self.gametickSent = self.gametick
        #if self.gametick > 590:
        #    print "-"
        #    print "-"

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    """
       d8b          d8b
       Y8P          Y8P

      8888  .d88b.  888 88888b.       888d888 8888b.   .d8888b .d88b.
      "888 d88""88b 888 888 "88b      888P"      "88b d88P"   d8P  Y8b
       888 888  888 888 888  888      888    .d888888 888     88888888
       888 Y88..88P 888 888  888      888    888  888 Y88b.   Y8b.
       888  "Y88P"  888 888  888      888    "Y888888  "Y8888P "Y8888
       888
      d88P
    888P"
    """
    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self):
        if self.name[-2:] in self.availableTracks:
            track = self.availableTracks[self.name[-2:]]
            print "SELECTED:", track
        else:
            track = 'keimola'
        return self.msg("joinRace", {"botId": {
            "name": self.name,
            "key": self.key
            },
            "trackName": track,
            "password": "bots",
            "carCount": 4
            })

    def join_race_ke(self):
        return self.msg("joinRace", {"botId": {
            "name": self.name,
            "key": self.key
            },
            "trackName": "keimola",
            "password": "jamcon",
            "carCount": 1
            })

    def join_race_ge(self):
        return self.msg("joinRace", {"botId": {
            "name": self.name,
            "key": self.key
            },
            "trackName": "germany",
            "password": "jamcon",
            "carCount": 1
            })

    def join_race_us(self):
        return self.msg("joinRace", {"botId": {
            "name": self.name,
            "key": self.key
            },
            "trackName": "usa",
            "password": "jamcon",
            "carCount": 1
            })

    def join_race_fr(self):
        return self.msg("joinRace", {"botId": {
            "name": self.name,
            "key": self.key
            },
            "trackName": "france",
            "password": "jamcon",
            "carCount": 1
            })

    def join_race_el(self):
        return self.msg("joinRace", {"botId": {
            "name": self.name,
            "key": self.key
            },
            "trackName": "elaeintarha",
            "password": "jamcon",
            "carCount": 1
            })

    def join_race_im(self):
        return self.msg("joinRace", {"botId": {
            "name": self.name,
            "key": self.key
            },
            "trackName": "imola",
            "password": "jamcon",
            "carCount": 1
            })

    def join_race_en(self):
        return self.msg("joinRace", {"botId": {
            "name": self.name,
            "key": self.key
            },
            "trackName": "england",
            "password": "jamcon",
            "carCount": 1
            })

    def join_race_su(self):
        return self.msg("joinRace", {"botId": {
            "name": self.name,
            "key": self.key
            },
            "trackName": "suzuka",
            "password": "jamcon",
            "carCount": 1
            })

    def join_race_pe(self):
        return self.msg("joinRace", {"botId": {
            "name": self.name,
            "key": self.key
            },
            "trackName": "pentag",
            "password": "jamcon",
            "carCount": 1
            })

    def on_your_car(self, data):
        print data['color'], "goes fastest!"

    def throttle(self, throttle):
        # ensure the throttle is in a good range
        throttle = max(min(1.0,throttle),0.0)
        self.msg("throttle", throttle)

    def lane_change(self, direction):
        self.msg("switchLane", direction)
        #self.lane = direction
        #if direction == "Left":
            #self.debugMessage = self.debugMessage + "LEFT REQUEST"
            #if self.cars[self.name]['now']['lane'] > 0:
            #    self.cars[self.name]['next']['lane'] = self.cars[self.name]['now']['lane'] - 1
        #else:
        #    self.debugMessage = self.debugMessage + "RIGHT REQUEST"
        #    if self.cars[self.name]['now']['lane'] < len(self.track[0]['lanes'])-1:
        #        self.cars[self.name]['next']['lane'] = self.cars[self.name]['now']['lane'] + 1
        #print "lane change ", direction

    def set_turbo(self):
        self.msg("turbo", "SHAZOOOOOOOOOM!!")
        #self.turboAvailable = False
        self.cars[self.name]['turboAvailable'] = False
        print "SHAZOOOOOOOOOM!!: for ", self.turboData['turboDurationTicks'], " at:", self.turboData['turboFactor']

    def ping(self):
        self.msg("ping", {})

    def run(self):
        if self.DEBUG:
            self.join_race()
        else:
            self.join()
        self.msg_loop()

    """
                                                                         d8b          d8b 888
                                                                         Y8P          Y8P 888
                                                                                          888
     .d88b.  88888b.        .d88b.   8888b.  88888b.d88b.   .d88b.       888 88888b.  888 888888
    d88""88b 888 "88b      d88P"88b     "88b 888 "888 "88b d8P  Y8b      888 888 "88b 888 888
    888  888 888  888      888  888 .d888888 888  888  888 88888888      888 888  888 888 888
    Y88..88P 888  888      Y88b 888 888  888 888  888  888 Y8b.          888 888  888 888 Y88b.
     "Y88P"  888  888       "Y88888 "Y888888 888  888  888  "Y8888       888 888  888 888  "Y888
                                888
                           Y8b d88P
                            "Y88P"
    """
    def on_game_init(self, data):
        # let's go through all the pieces
        # and do these things:
        # - get the length
        # - get the angle and radius (do this last)
        # - mark what section we are in, increment at each switch

        print("Game Init")
        #if self.DEBUG:
        #    print json.dumps(data)

        lanes = data['race']['track']['lanes']
        pieces = data['race']['track']['pieces']

        # total laps
        if 'laps' in data['race']['raceSession']:
            self.totalLaps = data['race']['raceSession']['laps']
            self.qualifydurationMs = 0
            print self.totalLaps, "lap race"
        else:
            print "qualify laps no laps data"
            self.qualifydurationMs = data['race']['raceSession']['durationMs']
            self.totalLaps = -1
            #print json.dumps(data)


        accumulated = []
        for l in lanes:
            accumulated.append(0)

        # total pieces
        self.totalPieces = len(pieces)
        print "total track pieces", self.totalPieces

        pSwitch = -1
        currentLane = -1
        # self.track = []
        if not self.track:
            for p in range(len(pieces)):
                tLanes = []
                for l in lanes:
                    # a straight
                    if "length" in pieces[p]:
                        direction = "STRAIGHT"
                        # we have a straight
                        accumulated[l['index']] = pieces[p]['length'] + accumulated[l['index']]

                        tLane = {"length": pieces[p]['length'],
                            "radius": 0,
                            "angle": 0,
                            "accum": accumulated[l['index']]}

                        tLanes.append(tLane)

                    # a corner
                    if "angle" in pieces[p]:
                        # we have a corner
                        angle = pieces[p]['angle']
                        if angle > 0:
                            # righty tighty
                            direction = "RIGHT"
                            radius = pieces[p]['radius'] - l['distanceFromCenter']
                        else:
                            # lefty loosy
                            direction = "LEFT"
                            radius = pieces[p]['radius'] + l['distanceFromCenter']

                        length = 2 * math.pi * radius * abs(angle) / 360.0

                        accumulated[l['index']] = length + accumulated[l['index']]

                        tLane = {"length": length,
                            "radius": radius,
                            "angle": angle,
                            "accum": accumulated[l['index']]}

                        tLanes.append(tLane)

                # get switch
                #if 'switch' in pieces[p]:
#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################
#####################################################################
                if ('switch' in pieces[p]) and (direction == 'STRAIGHT'):
                    sw = pieces[p]['switch']

                    lastSwitch = p
                    if pSwitch < 0:
                        firstSwitch = p
                    if pSwitch > 0:
                        # find the shortest
                        shortest = 0
                        for l in range(len(tLanes) - 1):
                            if tLanes[l+1]['accum'] < tLanes[shortest]['accum']:
                                shortest = l+1
                        #tell to change on the previous piece
                        self.laneChanges[pSwitch-1] = shortest

                    pSwitch = p
                    # reset accumulated
                    for i in range(len(accumulated)):
                        accumulated[i] = 0
                else:
                    sw = False

                #add all the info
                self.track.append({"lanes": tLanes,
                        "switch": sw,
                        "type": direction})

            # fix the first switch
            if self.track[-1]['switch']:
                # just calculate the start of the track to a switch
                # find the shortest
                shortest = 0
                for l in range(len(self.track[firstSwitch]['lanes']) - 1):
                    if self.track[firstSwitch]['lanes'][l+1]['accum'] < self.track[firstSwitch]['lanes'][shortest]['accum']:
                        shortest = l+1

                self.laneChanges[lastSwitch-1] = shortest
            else:
                # have to do the last part of the track and the start
                # find the shortest
                shortest = 0
                for l in range(len(self.track[firstSwitch]['lanes']) - 1):
                    if self.track[firstSwitch]['lanes'][l+1]['accum'] + self.track[len(self.track)-1]['lanes'][l+1]['accum'] < self.track[firstSwitch]['lanes'][shortest]['accum'] + self.track[len(self.track)-1]['lanes'][shortest]['accum']:
                        shortest = l+1

                self.laneChanges[lastSwitch-1] = shortest

        if self.DEBUG:
            print json.dumps(self.laneChanges)
            # print the track info
            for p in self.track:
                print json.dumps(p)
        # find the turbo
        self.find_turbo_straight()
        # find the home straight
        self.find_home_straight()


    def on_join(self, data):
        print("Joined")
        #self.ping()

    def on_join_race(self, data):
        print "Joined Race"
        #self.ping()

    def on_create_race(self, data):
        print "Race created", data
        #self.ping()

    def on_game_start(self, data):
        print("Race started")
        print "racestart", json.dumps(data)
        self.racing = True
        print "Peddle to the metal: ", 1.0, self.gametick
        #self.throttle(1.0)
        #self.ping()

    """
    888                       d8b
    888                       Y8P
    888
    88888b.   .d88b. 88888888 888  .d88b.  888d888
    888 "88b d8P  Y8b   d88P  888 d8P  Y8b 888P"
    888  888 88888888  d88P   888 88888888 888
    888 d88P Y8b.     d88P    888 Y8b.     888
    88888P"   "Y8888 88888888 888  "Y8888  888
    """
    def get_bezier_point_by_param(self,t, p):
        """Gets coordinate of Bezier curve point by parameter 't'
        Returns point of a curve.

        Keyword arguments:
        t -- paramerter 0-1
        p -- array of x,y contol points
        """

        #checkParameterT(t);
        cx = 3.0 * (float(p[1][0]) - float(p[0][0]))
        bx = 3.0 * (float(p[2][0]) - float(p[1][0])) - cx
        ax = float(p[3][0]) - float(p[0][0]) - cx - bx
        
        cy = 3.0 * (float(p[1][1]) - float(p[0][1]))
        by = 3.0 * (float(p[2][1]) - float(p[1][1])) - cy
        ay = float(p[3][1]) - float(p[0][1]) - cy - by
        
        tSquared = t * t
        tCubed = tSquared * t
        resultX = (ax * tCubed) + (bx * tSquared) + (cx * t) + float(p[0][0])
        resultY = (ay * tCubed) + (by * tSquared) + (cy * t) + float(p[0][1])

        return [resultX, resultY]

    def get_bezier_derivate(self,t,p):
        """Gets 1st derivate of a Bezier curve in a point specified by parameter
        Returns vector of 1st Bezier curve derivate in a specified point.

        Keyword arguments:
        t -- paramerter 0-1
        p -- array of x,y contol points
        """
        #checkParameterT(t);

        tSquared = t * t
        s0 = -3.0 + 6.0 * t - 3.0 * tSquared
        s1 = 3.0 - 12.0 * t + 9.0 * tSquared
        s2 = 6.0 * t - 9.0 * tSquared
        s3 = 3.0 * tSquared
        resultX = float(p[0][0]) * s0 + float(p[1][0]) * s1 + float(p[2][0]) * s2 + float(p[3][0]) * s3
        resultY = float(p[0][1]) * s0 + float(p[1][1]) * s1 + float(p[2][1]) * s2 + float(p[3][1]) * s3

        return [resultX, resultY]

    def  get_bezier_second_derivate(self,t,p):
        """Gets 2nd derivate of a Bezier curve in a point specified by parameter t.
        Returns Vector of 2nd Bezier curve derivate in a specified point.

        Keyword arguments:
        t -- paramerter 0-1
        p -- array of x,y contol points
        """

        #checkParameterT(t);

        s0 = 6.0 - 6.0 * t
        s1 = -12.0 + 18.0 * t
        s2 = 6.0 - 18.0 * t
        s3 = 6.0 * t
        resultX = float(p[0][0]) * s0 + float(p[1][0]) * s1 + float(p[2][0]) * s2 + float(p[3][0]) * s3
        resultY = float(p[0][1]) * s0 + float(p[1][1]) * s1 + float(p[2][1]) * s2 + float(p[3][1]) * s3
        return [resultX, resultY]

    def get_bezier_radius(self,t,p):
        """Gets curvature radius of a Bezier curve in specified point.
        Returns Curvature radius.
        Curvature = 1/CurvatureRadius.

        Keyword arguments:
        t -- paramerter 0-1
        p -- array of x,y contol points
        """

        #checkParameterT(t);

        d1 = self.get_bezier_derivate(t,p)
        d2 = self.get_bezier_second_derivate(t,p)

        r1 = math.sqrt((float(d1[0]) * float(d1[0]) + float(d1[1]) * float(d1[1]))**3.0)
        r2 = abs(float(d1[0]) * float(d2[1]) - float(d2[0]) * float(d1[1]))
        #print r1,r2
        if r2 < 0.000000001:
            return 9999999999
        else:
            return r1 / r2

    def B1(self,t):
        return t*t*t

    def B2(self,t):
        return 3.0*t*t*(1.0-t)

    def B3(self,t):
        return 3.0*t*(1.0-t)*(1.0-t)

    def B4(self,t):
        return (1.0-t)*(1.0-t)*(1.0-t)

    def bezier(self,startX, startY, control1X, control1Y, control2X, control2Y, endX, endY, t):
        x = startX * self.B1(t) + control1X * self.B2(t) + control2X * self.B3(t) + endX * self.B4(t)
        y = startY * self.B1(t) + control1Y * self.B2(t) + control2Y * self.B3(t) + endY * self.B4(t)
        b = [x,y]
        return b

    def beziertest(self, pos):
        #print "bezier"
        #staight
        #startX=0.0 # laneA.startX
        #startY=0.0 # laneA.startY
        #endX=100.0 # laneB.endX
        #endY=20.0 # laneB.endY
        #corner
        radA = 110.0
        radB = 90.0
        startX=0.0 # laneA.startX
        startY=110.0 # laneA.startY
        endX=radB * math.sin(math.radians(45.0)) # laneB.endX
        endY=radB * math.cos(math.radians(45.0)) # laneB.endY
        """midAX = radA * math.sin(math.radians(45.0)) / 2.0 # laneB.endX
        midAY = radA * math.cos(math.radians(45.0)) / 2.0 # laneB.endX
        midBX = endX / 2.0
        midBY = endY / 2.0"""
        midAX = radA * math.sin(math.radians(22.5)) # laneB.endX
        midAY = radA * math.cos(math.radians(22.5)) # laneB.endX
        midBX = radB * math.sin(math.radians(22.5)) # laneB.endX
        midBY = radB * math.cos(math.radians(22.5)) # laneB.endX


        #straight
        #startControl=self.bezier(startX,startY,50.0,0.0,50.0,20.0,endX,endY,0.75)# controlPoint1
        #endControl=self.bezier(startX,startY,50.0,0.0,50.0,20.0,endX,endY,0.25)# controlPoint2
        #corner
        startControl=self.bezier(startX,startY,midAX,midAY,midBX,midBY,endX,endY,0.75)# controlPoint1
        endControl=self.bezier(startX,startY,midAX,midAY,midBX,midBY,endX,endY,0.25)# controlPoint2

        curve = [[startX,startY],startControl,endControl,[endX,endY]]
        dist = 81.02948414200813
        t = pos/dist
        #print curve, self.get_bezier_radius(t,curve)
        rad = self.get_bezier_radius(t,curve)
        print self.get_bezier_point_by_param(t, curve), rad,
        return rad
        #for i in range(1,100):
        #    print self.get_bezier_point_by_param(i/100.0,curve),
        #    print radA * math.sin(math.radians(i*45.0/100.0)), radA * math.cos(math.radians(i*45.0/100.0)),
        #    print radB * math.sin(math.radians(i*45.0/100.0)), radB * math.cos(math.radians(i*45.0/100.0)),
        #    print self.get_bezier_radius(i/100.0,curve)

        """var cubic = _.partial(bezier,
                        laneA.startX, laneA.startY,
                        laneA.midX, laneA.midY,
                        laneB.midX, laneB.midY,
                        laneB.endX, laneB.endY)

        var controlPoint1 = cubic(0.75)
        var controlPoint2 = cubic(0.25)"""


    """
                           888          888                                        888    d8b                             888          888
                           888          888                                        888    Y8P                             888          888
                           888          888                                        888                                    888          888
    888  888 88888b.   .d88888  8888b.  888888 .d88b.       88888b.d88b.   .d88b.  888888 888  .d88b.  88888b.        .d88888  8888b.  888888  8888b.
    888  888 888 "88b d88" 888     "88b 888   d8P  Y8b      888 "888 "88b d88""88b 888    888 d88""88b 888 "88b      d88" 888     "88b 888        "88b
    888  888 888  888 888  888 .d888888 888   88888888      888  888  888 888  888 888    888 888  888 888  888      888  888 .d888888 888    .d888888
    Y88b 888 888 d88P Y88b 888 888  888 Y88b. Y8b.          888  888  888 Y88..88P Y88b.  888 Y88..88P 888  888      Y88b 888 888  888 Y88b.  888  888
     "Y88888 88888P"   "Y88888 "Y888888  "Y888 "Y8888       888  888  888  "Y88P"   "Y888 888  "Y88P"  888  888       "Y88888 "Y888888  "Y888 "Y888888
             888
             888
             888
     """
    def update_motion_data(self, racer):
        """Update motion data for given racer

        Keyword arguments:
        racer -- object containing racer data
        """
        # update motion data like distance, velocity and acceleration

        if self.gametick <= 0:
            print "gameTick == 0"
            # correct old
            self.pPos = self.cPos
            self.pTile = self.cTile
            self.pAngle = abs(self.cAngle)
            # get new
            self.cTile = racer['piecePosition']['pieceIndex']
            self.cPos = racer['piecePosition']['inPieceDistance']
            self.cAngle = racer['angle']

            currentLane = racer['piecePosition']['lane']['startLaneIndex']
            self.cLane = currentLane
            self.pLane = currentLane

        else:
            if self.gametick == 1:
                print "gameTick == 1"
                # add  this here so on race we have myLane
                currentLane = racer['piecePosition']['lane']['startLaneIndex']
                self.cLane = currentLane
                self.pLane = currentLane
            # correct old
            self.pPos = self.cPos
            self.pTile = self.cTile
            self.pAngle = self.cAngle
            self.pLane = self.cLane
            # get new
            self.cTile = racer['piecePosition']['pieceIndex']
            self.cPos = racer['piecePosition']['inPieceDistance']
            self.cAngle = racer['angle']
            self.cLane = racer['piecePosition']['lane']['startLaneIndex']

            #next tile
            if self.cTile+1 == len(self.track):
                self.nTile = 0
            else:
                self.nTile = self.cTile + 1

            # total lap distance
            if self.lap != racer['piecePosition']['lap']:
                # new lap so fix the lap distance
                self.cLapDistance = self.cPos
            else:
                # normal lap
                if self.cTile == self.pTile:
                    self.cLapDistance = self.cLapDistance + (self.cPos-self.pPos)
                else:
                    # (length of previous tile - pPos) + cPos + clapdistane
                    # a bit wrong as we don't know the length of the switches but who cares
                    self.cLapDistance = self.cLapDistance + self.cPos + (self.track[self.pTile]['lanes'][self.pLane]['length']-self.pPos)

            # update lap
            self.lap = racer['piecePosition']['lap']

            # update velocity and acceleration depending on the change of tile
            if self.cTile == self.pTile:
                # update acceleration
                self.a = (self.cPos - self.pPos) - self.v
                # update velocity
                self.v = self.cPos - self.pPos
            else:
                # a bit wrong as we don't know the length of the switches so if it was a switch we skip and leave the last
                """if self.track[self.pTile]['switch']:
                    #self.a we leave the same
                    # to update velocity we use the previous acceleration
                    self.v = self.v + self.a
                else:"""
                # update acceleration
                self.a = ((self.track[self.pTile]['lanes'][self.pLane]['length'] + self.cPos) - self.pPos) - self.v
                # update velocity
                self.v = (self.track[self.pTile]['lanes'][self.pLane]['length'] + self.cPos) - self.pPos

            # update max velocity
            if self.v > self.vmax:
                self.vmax = self.v

            # update max acceleration
            if self.a > self.amax:
                self.amax = self.a


            #update angle info
            self.pAngleRate = self.cAngleRate
            self.pAngleRateRate = self.cAngleRateRate

            self.cAngleRate = self.cAngle - self.pAngle
            self.cAngleRateRate = self.cAngleRate - self.pAngleRate
        

    """
                      888          .d888                            888    d8b
                      888         d88P"                             888    Y8P
                      888         888                               888
     .d88b.   .d88b.  888888      888888 888  888 88888b.   .d8888b 888888 888  .d88b.  88888b.  .d8888b
    d88P"88b d8P  Y8b 888         888    888  888 888 "88b d88P"    888    888 d88""88b 888 "88b 88K
    888  888 88888888 888         888    888  888 888  888 888      888    888 888  888 888  888 "Y8888b.
    Y88b 888 Y8b.     Y88b.       888    Y88b 888 888  888 Y88b.    Y88b.  888 Y88..88P 888  888      X88
     "Y88888  "Y8888   "Y888      888     "Y88888 888  888  "Y8888P  "Y888 888  "Y88P"  888  888  88888P'
         888
    Y8b d88P
     "Y88P"
    """
    def get_x(self, carname="", time='now'):
        """Get the x position in the tile

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        """
        if carname == "":
            carname = self.name
        x = self.cars[carname][time]['px']
        return x

    def get_v(self, carname="", time='now'):
        """Get the velocity

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        """
        if carname == "":
            carname = self.name
        v = self.cars[carname][time]['v']
        return v

    def get_pv(self, carname="", time='now'):
        """Get the velocity

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        """
        if carname == "":
            carname = self.name
        pv = self.cars[carname][time]['pv']
        return pv

    def get_a(self, carname="", time='now'):
        """Get the acceleration

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        """
        if carname == "":
            carname = self.name
        a = self.cars[carname][time]['a']
        return a

    def get_theta(self, carname="", time='now'):
        """Get the angle

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        """
        if carname == "":
            carname = self.name
        t = self.cars[carname][time]['t']
        return t

    def get_w(self, carname="", time='now'):
        """Get the angular velocity

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        """
        if carname == "":
            carname = self.name
        w = self.cars[carname][time]['w']
        return w

    def get_alpha(self, carname="", time='now'):
        """Get the angular acceleration

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        """
        if carname == "":
            carname = self.name
        alpha = self.cars[carname][time]['aa']
        return alpha

    def get_lap(self, carname="", time='now'):
        """Get the lap

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        """
        if carname == "":
            carname = self.name
        lap = self.cars[carname][time]['lap']
        return lap

    def get_tile(self, carname="", time='now'):
        """Get the tile

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        """
        if carname == "":
            carname = self.name
        tile = self.cars[carname][time]['tile']
        return tile

    def get_lane(self, carname="", time='now'):
        """Get the tile

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        """
        if carname == "":
            carname = self.name
        lane = self.cars[carname][time]['lane']
        return lane

    def get_next_lane(self, carname="", time='now'):
        """Get the tile

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        """
        if carname == "":
            carname = self.name
        lane = self.cars[carname][time]['nextlane']
        return lane

    def get_radius(self, carname="", time='now', tile=-1, startLaneIndex=-1):
        """Get the corner radius

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        tile -- the tile (default current tile)
        lane -- the lane (default current lane)
        """

        if carname == "":
            carname = self.name
        if tile == -1:
            #tile = self.cars[carname]['now']['tile']
            tile = self.get_tile(carname=carname,time=time)
        elif tile >= len(self.track):
            tile = tile - len(self.track)
        if startLaneIndex == -1 or startLaneIndex >= len(self.track[0]['lanes']):
            #startLaneIndex = self.cars[carname]['now']['lane']
            startLaneIndex = self.get_lane(carname=carname,time=time)

        radius = self.track[tile]['lanes'][startLaneIndex]['radius']

        return radius

    def get_type(self, carname="", time='now', tile=-1):
        """Get the tile type

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        tile -- the tile (default current tile)
        """

        if carname == "":
            carname = self.name
        if tile == -1:
            #tile = self.cars[carname]['now']['tile']
            tile = self.get_tile(carname=carname,time=time)
        elif tile >= len(self.track):
            tile = tile - len(self.track)

        t = self.track[tile]['type']

        return t

    def get_switch(self, carname="", time='now', tile=-1):
        """Return true if it is a switch

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        tile -- the tile (default current tile)
        """

        if carname == "":
            carname = self.name
        if tile == -1:
            #tile = self.cars[carname]['now']['tile']
            tile = self.get_tile(carname=carname,time=time)
        elif tile >= len(self.track):
            tile = tile - len(self.track)

        s = self.track[tile]['switch']

        return s

    def get_switch_length(self, tile, startLaneIndex, endLaneIndex):
        """Get the tile length during switch

        Keyword arguments:
        tile -- the tile 
        startLaneIndex -- the start lane
        endLaneIndex -- the end lane
        """
        if self.get_switch(tile=tile):
            # it is a switch so lets find it in the data
            if self.get_type(tile=tile) == 'STRAIGHT':
                # we use length not radius
                if self.get_length(tile=tile) in self.switchLengths[0]:
                    return self.switchLengths[0][self.get_length(tile=tile)][self.get_length(tile=tile)]
                else:
                    print "NOT FOUND - NEED TO ADD straight length"
            else:
                # a corner
                ang = abs(self.track[tile]['lanes'][0]['angle'])
                if ang in self.switchLengths:
                    if self.get_length(tile=tile, startLaneIndex=startLaneIndex) in self.switchLengths[ang]:
                        if self.get_length(tile=tile, startLaneIndex=endLaneIndex) in self.switchLengths[ang][self.get_length(tile=tile, startLaneIndex=startLaneIndex) ]:
                            return self.switchLengths[ang][self.get_length(tile=tile, startLaneIndex=startLaneIndex)][self.get_length(tile=tile, startLaneIndex=endLaneIndex)]
                        else:
                            print "NOT FOUND - NEED TO ADD exit length"
                    else:
                        print "NOT FOUND - NEED TO ADD entry and exit length"
                else:
                    print "NOT FOUND - NEED TO ADD angle"
            # return half start lane length if 
            return (self.get_length(tile=tile, startLaneIndex=startLaneIndex) + self.get_length(tile=tile, startLaneIndex=endLaneIndex))/2.0
        else:
            print "not a switch"
            return self.get_length(tile=tile, startLaneIndex=startLaneIndex)

    def get_length(self, carname="", time='now', tile=-1, startLaneIndex=-1, endLaneIndex=-1):
        """Get the tile length

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        tile -- the tile (default current tile)
        startLaneIndex -- the lane (default current lane) *only applies if this tile is a switch
        endLaneIndex -- the lane (default current lane) *only applies if this tile is a switch
        """

        if carname == "":
            carname = self.name
        if tile == -1:
            #tile = self.cars[carname]['now']['tile']
            tile = self.get_tile(carname=carname,time=time)
        elif tile >= len(self.track):
            tile = tile - len(self.track)
        
        if startLaneIndex == -1 or startLaneIndex >= len(self.track[0]['lanes']):
            startLaneIndex = self.get_lane(carname=carname,time=time)
            #no switch desired, probably just tile length
            length = self.track[tile]['lanes'][startLaneIndex]['length']
        elif startLaneIndex == endLaneIndex:
            #no switch desired
            length = self.track[tile]['lanes'][startLaneIndex]['length']
        elif endLaneIndex == -1 or endLaneIndex >= len(self.track[0]['lanes']):
            # we can't do a switch if end land is bollocks
            length = self.track[tile]['lanes'][startLaneIndex]['length']    
        else:
            # need to get switch length
            length = self.get_switch_length(tile=tile, startLaneIndex=startLaneIndex, endLaneIndex=endLaneIndex)

        return length

    def get_turboing(self, carname="", tick=-1):
        """Get if the car is turboing

        Keyword arguments:
        carname -- the car name (default self.name)
        time -- the time (default now)
        """
        if carname == "":
            carname = self.name
        if tick == -1:
            return self.cars[carname]['turboing']

        if tick >= self.cars[carname]['turboStartTick'] and tick <= self.cars[carname]['turboEndTick']:
            turboing = True
        else:
            turboing = False
        return turboing

    def get_turbo_available(self, carname=""):
        """Return True if turbo is available

        Keyword arguments:
        carname -- the car name (default self.name)
        """
        if carname == "":
            carname = self.name

        available = self.cars[carname]['turboAvailable']
        return available

    """
                      888                                d8b 888            888
                      888                                Y8P 888            888
                      888                                    888            888
     .d88b.   .d88b.  888888      .d8888b  888  888  888 888 888888 .d8888b 88888b.
    d88P"88b d8P  Y8b 888         88K      888  888  888 888 888   d88P"    888 "88b
    888  888 88888888 888         "Y8888b. 888  888  888 888 888   888      888  888
    Y88b 888 Y8b.     Y88b.            X88 Y88b 888 d88P 888 Y88b. Y88b.    888  888
     "Y88888  "Y8888   "Y888       88888P'  "Y8888888P"  888  "Y888 "Y8888P 888  888
         888
    Y8b d88P
     "Y88P"
    """
    def get_next_switch(self, carname="", tile=-1):
        """Get the next switch tile

        Keyword arguments:
        carname -- the car name (default self.name)
        tile -- the tile (default current tile)
        """

        if carname == "":
            carname = self.name
        if tile == -1:
            tile = self.cars[carname]['now']['tile']
        elif tile >= len(self.track):
            tile = tile - len(self.track)
        # from tile to end
        for t in range(tile,len(self.track)-1):
            if self.track[t]['switch']:
                return t
        # from start to tile
        for t in range(0,tile):
            if self.track[t]['switch']:
                return t
        # if none found
        return -1


    """
                      888         888    888                      888    888    888
                      888         888    888                      888    888    888
                      888         888    888                      888    888    888
     .d88b.   .d88b.  888888      888888 88888b.  888d888 .d88b.  888888 888888 888  .d88b.
    d88P"88b d8P  Y8b 888         888    888 "88b 888P"  d88""88b 888    888    888 d8P  Y8b
    888  888 88888888 888         888    888  888 888    888  888 888    888    888 88888888
    Y88b 888 Y8b.     Y88b.       Y88b.  888  888 888    Y88..88P Y88b.  Y88b.  888 Y8b.
     "Y88888  "Y8888   "Y888       "Y888 888  888 888     "Y88P"   "Y888  "Y888 888  "Y8888
         888
    Y8b d88P
     "Y88P"
     """
    def get_throttle(self, velocity):
        """Return throttle required to get to velocity

        Keyword arguments:
        velocity -- the final velocity
        """
        # avoid divide by zero error
        if self.k1 == 0 or self.k2 == 0:
            return 1.0

        v = self.cars[self.name]['now']['v']
        #v = self.predict_velocity()

        throttle = (velocity + (v*(self.k2-1.0)))/self.k1
        #print "gas",throttle, self.gametick
        throttle = max(min(1.0,throttle),0.0)

        return throttle

    """
                                  888 d8b 888            888          .d888                            888    d8b
                                  888 Y8P 888            888         d88P"                             888    Y8P
                                  888     888            888         888                               888
    88888b.  888d888 .d88b.   .d88888 888 888888 .d8888b 888888      888888 888  888 88888b.   .d8888b 888888 888  .d88b.  88888b.  .d8888b
    888 "88b 888P"  d8P  Y8b d88" 888 888 888   d88P"    888         888    888  888 888 "88b d88P"    888    888 d88""88b 888 "88b 88K
    888  888 888    88888888 888  888 888 888   888      888         888    888  888 888  888 888      888    888 888  888 888  888 "Y8888b.
    888 d88P 888    Y8b.     Y88b 888 888 Y88b. Y88b.    Y88b.       888    Y88b 888 888  888 Y88b.    Y88b.  888 Y88..88P 888  888      X88
    88888P"  888     "Y8888   "Y88888 888  "Y888 "Y8888P  "Y888      888     "Y88888 888  888  "Y8888P  "Y888 888  "Y88P"  888  888  88888P'
    888
    888
    888
     """
    def predict_one_tick(self, d, turboForce=False):
        """Predict values for next tick
        Use and save them to the ideal dictionary

        Keyword arguments:
        d -- data to use for prediction
            dictionary with x,v,a,t,w,alpha,tile,startLaneIndex,endLaneIndex,r,gas
        turboForce -- force to predict with turbo
        """
        if self.k == 0:
            k1 = 0.2
            k2 = 0.02
        else:
            k1 = self.k1
            k2 = self.k2

        if self.slipFound:
            W = self.W
            T = self.T
            A = self.A
            B = self.B
        else:
            W = 0.1
            T = 0.00125
            A = 0.53033008589
            B = 0.3

        if turboForce:
            onTurbo = True
        else:
            onTurbo = self.get_turboing(tick=d['tick'])
        radius = self.get_radius(tile=d['tile'],startLaneIndex=d['startLaneIndex'])
        d['r'] = radius
        alpha = (-T*d['v']*d['t']) - (W*d['w'])
        torque = 0.0
        if self.track[d['tile']]['type'] != 'STRAIGHT' and d['v'] > (B * math.sqrt(radius) / A):
            # need to correct which to take
            #if d['t'] > 0.0:
            if self.track[d['tile']]['type'] == 'RIGHT':
                torque = (A * d['v'] * d['v'] / math.sqrt(radius)) - (B*d['v'])
            else:
                torque = (-1.0 * A * d['v'] * d['v'] / math.sqrt(radius)) + (B*d['v'])

        # angle
        d['alpha'] = alpha + torque
        d['w'] = d['w'] + d['alpha']
        d['t'] = d['t'] + d['w']

        # distance
        if onTurbo and 'turboFactor' in self.turboData:
            d['a'] = k1*d['gas']*self.turboData['turboFactor'] - k2*d['v']
        else:
            d['a'] = k1*d['gas'] - k2*d['v']
        d['v'] = d['v'] + d['a']
        d['x'] = d['x'] + d['v']

        # increment tile if required
        if d['x'] > self.get_length(tile=d['tile'], startLaneIndex=d['startLaneIndex'], endLaneIndex=d['endLaneIndex']):
            d['x'] = d['x'] - self.get_length(tile=d['tile'], startLaneIndex=d['startLaneIndex'], endLaneIndex=d['endLaneIndex'])
            # if this was a switch we need to update the lane
            if self.track[d['tile']]['switch'] and self.slipFound and d['startLaneIndex']!=d['endLaneIndex']:
                #if self.laneChanges[d['tile']-1] == 0:
                #    d['startLaneIndex'] -= 1
                #else:
                #    d['startLaneIndex'] += 1

                # change start lane to end
                #print "lane change from", d['startLaneIndex'], "to", d['endLaneIndex']
                d['startLaneIndex'] = int(d['endLaneIndex'])
            d['tile'] = (d['tile'] + 1) % len(self.track)

        d['tick'] += 1

        return d

    def predict_variable(self, carname="", variable='v'):
        """predict one step ahead for the given variable

        Keyword arguments:
        carname -- the car name (default self.name)
        variable -- the variable to predict (default v)
        """

        if carname == "":
            carname = self.name

        if self.timeConstant == 0 or self.maxSpeed == 0:
            return 0

        dnow = {}
        dnow['r']=self.get_radius(carname)
        dnow['x']=self.get_x(carname)
        dnow['v']=self.get_v(carname)
        dnow['a']=self.get_a(carname)
        dnow['t']=self.get_theta(carname)
        dnow['w']=self.get_w(carname)
        dnow['alpha']=self.get_alpha(carname)
        dnow['tile']=self.get_tile(carname)
        dnow['startLaneIndex']=self.get_lane(carname)
        dnow['endLaneIndex']=self.get_next_lane(carname)
        dnow['tick']=self.gametick
        dnow['gas']=self.theGas

        if variable in dnow:
            dnow=self.predict_one_tick(dnow)
            var = dnow[variable]
            return var
        else:
            return 0

    def predict_velocity(self, carname="", tile=-1):
        """predict one step ahead for velocity

        Keyword arguments:
        carname -- the car name (default self.name)
        tile -- the tile (default current tile)
        """

        if carname == "":
            carname = self.name
        if tile == -1:
            tile = self.cars[carname]['now']['tile']
        elif tile >= len(self.track):
            tile = tile - len(self.track)

        if self.k == 0:
            # use default if not found yet
            k1 = 0.2
            k2 = 0.02
        else:
            k1 = self.k1
            k2 = self.k2

        if self.timeConstant == 0 or self.maxSpeed == 0:
            return 0
        throttle = self.theGas
        oldthrottle = self.theLastGas
        maxspeed = self.maxSpeed * throttle
        # perhaps need to confirm this also add in for turbo
        """if oldthrottle > throttle:
            # we are braking
            throttleVelocity = self.maxSpeed * throttle
            velocity = (self.v - throttleVelocity) * math.exp(-1.0 / self.timeConstant) + throttleVelocity
            if oldthrottle > throttle:
                vplus = (oldthrottle - throttle) * self.k1
                velocity += vplus
        else:
            # we are accelerating
            velocity = maxspeed - ((maxspeed - self.v) * math.exp(-1.0 / self.timeConstant))
            if oldthrottle < throttle:
                vplus = (oldthrottle - throttle) * self.k1
                velocity += vplus
        """
        if self.get_turboing() and 'turboFactor' in self.turboData:
            a = k1*throttle*self.turboData['turboFactor'] - k2*self.get_v(carname=carname)
        else:
            a = k1*throttle - k2*self.get_v(carname=carname)
        velocity = self.get_v(carname=carname) + a

        return velocity

    """
                           888          888                                            888
                           888          888                                            888
                           888          888                                            888
    888  888 88888b.   .d88888  8888b.  888888 .d88b.        8888b.  88888b.   .d88b.  888  .d88b.  .d8888b
    888  888 888 "88b d88" 888     "88b 888   d8P  Y8b          "88b 888 "88b d88P"88b 888 d8P  Y8b 88K
    888  888 888  888 888  888 .d888888 888   88888888      .d888888 888  888 888  888 888 88888888 "Y8888b.
    Y88b 888 888 d88P Y88b 888 888  888 Y88b. Y8b.          888  888 888  888 Y88b 888 888 Y8b.          X88
     "Y88888 88888P"   "Y88888 "Y888888  "Y888 "Y8888       "Y888888 888  888  "Y88888 888  "Y8888   88888P'
             888                                                                   888
             888                                                              Y8b d88P
             888                                                               "Y88P"
    SHOULD DELETE THIS
    """
    def update_angles(self, carname="", tile=-1):
        """Update the angle data

        Keyword arguments:
        carname -- the car name (default self.name)
        tile -- the tile (default current tile)
        """

        if carname == "":
            carname = self.name
        if tile == -1:
            tile = self.cars[carname]['now']['tile']
        elif tile >= len(self.track):
            tile = tile - len(self.track)

        if self.A == 0 and self.cAngleRateRate == 0:
            return 0.0
        elif self.A == 0 and self.cAngleRateRate != 0:
            #calculate A
            print "found the magic A",
            #A = 0.5303
            vel = self.v - self.a
            if self.track[self.cTile]['type'] == 'RIGHT':
                self.A = math.sqrt(self.get_radius()) * (abs(self.cAngleRateRate) + self.B*vel) / (vel * vel)
            else:
                self.A = -1.0 * math.sqrt(self.get_radius()) * (abs(self.cAngleRateRate) - self.B*vel) / (vel * vel)
            print self.A

        # need to add check for turbo
        radius = self.get_radius()
        alpha = (-self.T*self.v*self.cAngle) - (self.W*self.cAngleRate)
        torque = 0.0
        if self.track[self.cTile]['type'] != 'STRAIGHT' and self.v > (self.B * math.sqrt(radius) / self.A):
            if self.cAngle > 0.0:
                torque = (self.A * self.v * self.v / math.sqrt(radius)) - (self.B*self.v)
            else:
                torque = (-1.0*self.A * self.v * self.v / math.sqrt(radius)) + (self.B*self.v)

        alpha = alpha + torque
        return alpha

    """
                           888          888                                                                    888          888
                           888          888                                                                    888          888
                           888          888                                                                    888          888
    888  888 88888b.   .d88888  8888b.  888888 .d88b.       888d888 8888b.   .d8888b .d88b.  888d888       .d88888  8888b.  888888  8888b.
    888  888 888 "88b d88" 888     "88b 888   d8P  Y8b      888P"      "88b d88P"   d8P  Y8b 888P"        d88" 888     "88b 888        "88b
    888  888 888  888 888  888 .d888888 888   88888888      888    .d888888 888     88888888 888          888  888 .d888888 888    .d888888
    Y88b 888 888 d88P Y88b 888 888  888 Y88b. Y8b.          888    888  888 Y88b.   Y8b.     888          Y88b 888 888  888 Y88b.  888  888
     "Y88888 88888P"   "Y88888 "Y888888  "Y888 "Y8888       888    "Y888888  "Y8888P "Y8888  888           "Y88888 "Y888888  "Y888 "Y888888
             888
             888
             888
    """
    def update_racer_data(self, car):
        """Update all motion data as given by server
        This is for last, lastlast and now

        Keyword arguments:
        car -- the car data as given by server
        """
        ###DATALOG
        #dt = datetime.now()
        #print "start", dt.second, dt.microsecond
        name = car['id']['name']
        # create if it doesn't exist
        if name in self.cars:
            # last ###FIXME is this the fastest way to copy a dictionary
            self.cars[name]['lastlast'] = self.cars[name]['last'].copy()
            self.cars[name]['last'] = self.cars[name]['now'].copy()
            # update values
            self.cars[name]['now']['tile'] = car['piecePosition']['pieceIndex']
            self.cars[name]['now']['px'] = car['piecePosition']['inPieceDistance']
            self.cars[name]['now']['t'] = car['angle']
            self.cars[name]['now']['lane'] = car['piecePosition']['lane']['startLaneIndex']
            self.cars[name]['now']['nextlane'] = car['piecePosition']['lane']['endLaneIndex']
            self.cars[name]['now']['radius'] = self.track[self.cars[name]['now']['tile']]['lanes'][self.cars[name]['now']['lane']]['radius']

            #update the car positions
            if self.cars[name]['now']['tile'] in self.positions:
                # append
                self.positions[self.cars[name]['now']['tile']].append("" + name)
            else:
                self.positions[self.cars[name]['now']['tile']] = ["" + name]

            # total lap distance
            if car['piecePosition']['lap'] != self.cars[name]['now']['lap']:
                # new lap so fix the lap distance
                self.cars[name]['now']['lapx'] = self.cars[name]['now']['px']
            else:
                # normal lap
                if self.cars[name]['now']['tile'] == self.cars[name]['last']['tile']:
                    # lapx1 = lapx0 + (px1-px0)
                    self.cars[name]['now']['lapx'] = self.cars[name]['last']['lapx'] + (self.cars[name]['now']['px'] - self.cars[name]['last']['px'])
                else:
                    # (length of previous tile - pPos) + cPos + clapdistane
        ###FIXME a bit wrong as we don't know the length of the switches but who cares
                    self.cars[name]['now']['lapx'] = self.cars[name]['now']['lapx'] + self.cars[name]['now']['px'] + \
                        (self.track[self.cars[name]['last']['tile']]['lanes'][self.cars[name]['last']['lane']]['length'] - self.cars[name]['last']['px'])

            # update lap
            self.cars[name]['now']['lap'] = car['piecePosition']['lap']

            # update velocity and acceleration depending on the change of tile
            if self.cars[name]['now']['tile'] == self.cars[name]['last']['tile']:
                # update velocity
                self.cars[name]['now']['v'] = self.cars[name]['now']['px'] - self.cars[name]['last']['px']
                # update acceleration
                self.cars[name]['now']['a'] = self.cars[name]['now']['v'] - self.cars[name]['last']['v']
            else:
                # a bit wrong as we don't know the length of the switches so if it was a switch we skip and leave the last
                ###FIXME
                """this has been fixed but not tested"""
                # update velocity
                self.cars[name]['now']['v'] = (self.get_length(tile=self.cars[name]['last']['tile'], startLaneIndex=self.cars[name]['last']['lane'], endLaneIndex=self.cars[name]['last']['nextlane']) + \
                    self.cars[name]['now']['px']) - self.cars[name]['last']['px']
                # update acceleration
                self.cars[name]['now']['a'] = self.cars[name]['now']['v'] - self.cars[name]['last']['v']

            # update angle info
            self.cars[name]['now']['t'] = car['angle']
            self.cars[name]['now']['w'] = car['angle'] - self.cars[name]['last']['t']
            self.cars[name]['now']['aa'] = self.cars[name]['now']['w'] - self.cars[name]['last']['w']

        else:
            # we need to create and initialise the data
            self.cars[name] = {}
            self.cars[name]['now'] = {}
            self.cars[name]['last'] = {}
            self.cars[name]['next'] = {}
            # now
            self.cars[name]['now']['tile'] = car['piecePosition']['pieceIndex']
            self.cars[name]['now']['px'] = car['piecePosition']['inPieceDistance']
            self.cars[name]['now']['lapx'] = car['piecePosition']['inPieceDistance']
            self.cars[name]['now']['a'] = 0.0
            self.cars[name]['now']['v'] = 0.0
            self.cars[name]['now']['pv'] = 0.0
            self.cars[name]['now']['t'] = car['angle']
            self.cars[name]['now']['w'] = 0.0 #angular velocity
            self.cars[name]['now']['aa'] = 0.0 #angular acceleration
            self.cars[name]['now']['lane'] = car['piecePosition']['lane']['startLaneIndex']
            self.cars[name]['now']['nextlane'] = car['piecePosition']['lane']['endLaneIndex']
            self.cars[name]['now']['lap'] = car['piecePosition']['lap']
            self.cars[name]['now']['radius'] = self.track[self.cars[name]['now']['tile']]['lanes'][self.cars[name]['now']['lane']]['radius']
            # next
            self.cars[name]['next']['lane'] = car['piecePosition']['lane']['endLaneIndex']

            # last 
            self.cars[name]['last'] = self.cars[name]['now']
            # last last
            self.cars[name]['lastlast'] = self.cars[name]['last']

            self.cars[name]['racing'] = True #racing
            self.cars[name]['turboing'] = False
            self.cars[name]['turboAvailable'] = False
            self.cars[name]['turboStartTick'] = -1
            self.cars[name]['turboEndTick'] = -1

    def find_home_straight(self):
        """Find the Home Straight

        Keyword arguments:
        """

        self.homeStraightTile = 999
        # fix the last tracks to line up with the first
        print "going to fix the next section distances"
        t = len(self.track)-1
        while self.track[t]['type'] == 'STRAIGHT':
            # this is the boost on the last lap tile
            self.homeStraightTile = t
            t = t-1

        print "HOME STRAIGHT TILE is ", self.homeStraightTile


    def find_turbo_straight(self):
        """Find the Turbo Straight

        Keyword arguments:
        """

        # lets go forward through the tiles
        tdistance = 0
        self.turboStraight = 0
        possiblestart = 999
        possibledistance = 0

        for t in range(len(self.track)):
            if self.track[t]['type'] == 'STRAIGHT':
                # it is a straight add the length to the old distance
                if possibledistance == 0:
                    possiblestart = t
                possibledistance = possibledistance + self.track[t]['lanes'][0]['length']
            else:
                if possibledistance > tdistance:
                    tdistance = possibledistance
                    self.turboStraight = possiblestart
                possibledistance = 0
        #check crossing the finish line
        if self.track[-1]['type'] == 'STRAIGHT':
            # we need to continue
            t = 0
            while self.track[t]['type'] == 'STRAIGHT':
                possibledistance = possibledistance + self.track[t]['lanes'][0]['length']
                t = t + 1
            if possibledistance > tdistance:
                tdistance = possibledistance
                self.turboStraight = possiblestart
        print "TURBO STRAIGHT is at ", self.turboStraight


    def better_is_car_ahead(self):
        self.letsBumpem = False
        self.following = False
        self.whotofollow = ""

        # only need to check this if we are on a STRAIGHT
        ### FIXME perhaps not only on the straight
        if self.track[self.cars[self.name]['now']['tile']]['type'] == 'STRAIGHT' and self.gametick > 10 and self.timeConstant != 0:
            # we need to go through the self.cars for all the cars that are not us and mark if one is ahead and how far
            mylane = self.cars[self.name]['now']['lane']
            mytile = self.cars[self.name]['now']['tile']
            ahead = 999999
            carname = ""

            dtocorner = 0.0
            dtocar = 999999
            carFound = False

            # lets go forward through the tiles
            t = mytile
            while self.track[t]['type'] == 'STRAIGHT':
                if t in self.positions and not carFound:
                    #there is a car on this tile
                    #it could be me or someone else
                    if len(self.positions[t]) > 1:
                        #there are more than 1 car so we need to iterate
                        for x in range(len(self.positions[t])):
                            if self.positions[t][x] != self.name:
                                if self.cars[self.positions[t][x]]['now']['lane'] == self.cars[self.name]['now']['lane'] and \
                                    (dtocorner + self.cars[self.positions[t][x]]['now']['px'] - self.cars[self.name]['now']['px']) < dtocar and \
                                    (dtocorner + self.cars[self.positions[t][x]]['now']['px'] - self.cars[self.name]['now']['px']) > 0.0:
                                    dtocar = dtocorner + self.cars[self.positions[t][x]]['now']['px'] - self.cars[self.name]['now']['px']
                                    carFound = True
                                    carname = "" + self.positions[t][x]
                    elif t != mytile:
                        if self.cars[self.positions[t][0]]['now']['lane'] == self.cars[self.name]['now']['lane'] and \
                            (dtocorner + self.cars[self.positions[t][0]]['now']['px'] - self.cars[self.name]['now']['px']) < dtocar and \
                            (dtocorner + self.cars[self.positions[t][0]]['now']['px'] - self.cars[self.name]['now']['px']) > 0.0:
                            # found a sweet ride
                            carFound = True
                            dtocar = dtocorner + self.cars[self.positions[t][0]]['now']['px'] - self.cars[self.name]['now']['px']
                            carname = "" + self.positions[t][0]
                #update distance to corner now
                dtocorner = dtocorner + self.track[t]['lanes'][0]['length']



                #reset from start if needed
                if t+1 == len(self.track):
                    t = 0
                else:
                    t = t+1
            self.aheadx = dtocar

            if self.aheadx < 300 and carname != "":

                if self.DEBUG:
                    indistance = dtocorner - self.cars[self.name]['now']['px']
                    print "corner ahead", indistance, carname
                # calculate the time for the car in from to get to the next corner TimeToCorner
                TcVn = self.timeConstant * self.cars[carname]['now']['v']
                #print carname
                #print "TcVn", TcVn
                #print "time constants", self.timeConstant
                #print "v", self.cars[carname]['now']['v']

                if TcVn > 0.0:
                    #to ensure we don't divide by zero
                    logbit = (TcVn - (self.track[self.cars[carname]['now']['tile']]['nextSectionIn'] - self.cars[carname]['now']['px'])) / TcVn

                    if logbit > 0:
                        ttc = -1.0 * self.timeConstant * math.log(logbit)
                        # now use this in our acceleration equation to see if we can catch them
                        if self.cars[self.name]['turboing']:
                            myMax = self.maxSpeed * self.turboData['turboFactor']
                        else:
                            myMax = self.maxSpeed
                        d = (self.timeConstant * ( myMax - self.cars[self.name]['now']['v']) * math.exp(-1.0 * ttc / self.timeConstant)) + \
                            (myMax * ttc) - (self.timeConstant * (myMax - self.cars[self.name]['now']['v']))

                        # if the distance we can go in the time given is less then we have lets bump them off the track
                        if d + 40.0 < (self.track[self.cars[self.name]['now']['tile']]['nextSectionIn'] - self.cars[self.name]['now']['px']):
                            self.letsBumpem = True
                            self.following = True
                            self.whotofollow = carname
                            #if self.DEBUG:
                            #    print "Car", carname, "ahead by", ahead, "let's crash em"

    def best_is_car_ahead(self):
        self.letsBumpem = False
        self.following = False
        self.whotofollow = ""

        if self.gametick > 10 and self.timeConstant != 0:
            # we need to go through the self.cars for all the cars that are not us and mark if one is ahead and how far
            mylane = self.get_lane()
            mytile = self.get_tile()
            mylap = self.get_lap()
            myx = self.get_x()
            behindGroup = []
            aheadGroup = []
            ahead = 999999
            carname = ""


            dtocorner = 0.0
            dtocar = 999999
            carFound = False


            for n,car in self.cars.items():
                #print n, car
                # go through lap, then tile, then pos
                if n != self.name:
                    if self.get_lap(carname=n) < mylap:
                        behindGroup.append(n)
                    elif self.get_lap(carname=n) > mylap:
                        aheadGroup.append(n)
                    else:
                        #check tile
                        if self.get_tile(carname=n) < mytile:
                            behindGroup.append(n)
                        elif self.get_tile(carname=n) > mytile:
                            aheadGroup.append(n)
                        else:
                            #check x
                            if self.get_x(carname=n) < myx:
                                behindGroup.append(n)
                            elif self.get_x(carname=n) > myx:
                                aheadGroup.append(n)
                            else:
                                #assume behind
                                behindGroup.append(n)

            # now we have the behind group and ahead we need to decide on dodge or follow
            t = mytile
            while not carFound:
                if t in self.positions:
                    #there is a car on this tile
                    #it could be me or someone else
                    if len(self.positions[t]) > 1:
                        #there are more than 1 car so we need to iterate
                        for x in range(len(self.positions[t])):
                            if self.positions[t][x] != self.name:
                                if self.cars[self.positions[t][x]]['now']['lane'] == self.cars[self.name]['now']['lane'] and \
                                    (dtocorner + self.cars[self.positions[t][x]]['now']['px'] - self.cars[self.name]['now']['px']) < dtocar and \
                                    (dtocorner + self.cars[self.positions[t][x]]['now']['px'] - self.cars[self.name]['now']['px']) > 0.0:
                                    dtocar = dtocorner + self.cars[self.positions[t][x]]['now']['px'] - self.cars[self.name]['now']['px']
                                    carFound = True
                                    carname = "" + self.positions[t][x]
                    elif t != mytile:
                        if self.cars[self.positions[t][0]]['now']['lane'] == self.cars[self.name]['now']['lane'] and \
                            (dtocorner + self.cars[self.positions[t][0]]['now']['px'] - self.cars[self.name]['now']['px']) < dtocar and \
                            (dtocorner + self.cars[self.positions[t][0]]['now']['px'] - self.cars[self.name]['now']['px']) > 0.0:
                            # found a sweet ride
                            carFound = True
                            dtocar = dtocorner + self.cars[self.positions[t][0]]['now']['px'] - self.cars[self.name]['now']['px']
                            carname = "" + self.positions[t][0]
                #update distance to corner now
                dtocorner = dtocorner + self.track[t]['lanes'][0]['length']



                #reset from start if needed
                if t+1 == len(self.track):
                    t = 0
                else:
                    t = t+1
            self.aheadx = dtocar
            #print carname, self.aheadx






    def is_car_ahead(self):
        self.letsBumpem = False
        self.following = False
        self.whotofollow = ""
        # only need to check this if we are on a STRAIGHT
        ### FIXME perhaps not only on the straight
        if self.track[self.cars[self.name]['now']['tile']]['type'] == 'STRAIGHT' and self.gametick > 10 and self.timeConstant != 0:
            # we need to go through the self.cars for all the cars that are not us and mark if one is ahead and how far
            mylane = self.cars[self.name]['now']['lane']
            mylapx = self.cars[self.name]['now']['lapx']
            ahead = 999999
            carname = ""

            for n, car in self.cars.items():
                if car['racing'] and car['now']['lane'] == mylane and car['now']['lapx'] > mylapx and car['now']['lapx'] - mylapx < ahead and car['now']['v'] > 0:
                    ahead = car['now']['lapx'] - mylapx
                    carname = "" + n

            self.aheadx = ahead

            if self.aheadx < 300 and carname != "":
                # calculate the time for the car in from to get to the next corner TimeToCorner
                TcVn = self.timeConstant * self.cars[carname]['now']['v']
                #print carname
                #print "TcVn", TcVn
                #print "time constants", self.timeConstant
                #print "v", self.cars[carname]['now']['v']

                logbit = (TcVn - (self.track[self.cars[carname]['now']['tile']]['nextSectionIn'] - self.cars[carname]['now']['px'])) / TcVn

                if logbit > 0:
                    ttc = -1.0 * self.timeConstant * math.log(logbit)
                    # now use this in our acceleration equation to see if we can catch them
                    if self.cars[self.name]['turboing']:
                        myMax = self.maxSpeed * self.turboData['turboFactor']
                    else:
                        myMax = self.maxSpeed
                    d = (self.timeConstant * ( myMax - self.cars[self.name]['now']['v']) * math.exp(-1.0 * ttc / self.timeConstant)) + \
                        (myMax * ttc) - (self.timeConstant * (myMax - self.cars[self.name]['now']['v']))

                    # if the distance we can go in the time given is less then we have lets bump them off the track
                    if d + 40.0 < (self.track[self.cars[self.name]['now']['tile']]['nextSectionIn'] - self.cars[self.name]['now']['px']):
                        self.letsBumpem = True
                        self.following = True
                        self.whotofollow = carname
                        #if self.DEBUG:
                        #    print "Car", carname, "ahead by", ahead, "let's crash em"


    def update_corner_speeds(self):
        print "updating corner speeds with Fslip at ", self.Fslip

        # setup the variables to use here that we might not have yet
        if self.maxSpeed > 0:
            maxSpeed = self.maxSpeed
        else:
            maxSpeed = 10.0
            print "JUST USING 10.0 for the moment"

        if self.timeConstant > 0:
            Tc =  self.timeConstant
        else:
            Tc =  49.5
            print "JUST USING 49.5 for the moment"

        for l in range(len(self.track[0]['lanes'])):
            #lets do it for each lane
            nRadius = -1
            cRadius = self.track[-1]['lanes'][l]['radius']
            nextType = self.track[0]['type']
            if nextType == 'STRAIGHT':
                nextSpeed = maxSpeed
            else:
                #nextSpeed = math.sqrt(self.track[0]['lanes'][l]['radius'] * self.Fslip) + 1.0
                nextSpeed = math.sqrt(self.track[0]['lanes'][l]['radius'] * self.Fslip) + self.track[0]['multiplier']

            sectionDistance = 0
            for p in reversed(range(len(self.track))):
                # now go backwards through and complete
                if p+1 == len(self.track):
                    q = 0
                else:
                    q = p+1
                #print json.dumps(p)
                nRadius = cRadius
                cRadius = self.track[p]['lanes'][l]['radius']
                # same so add some distance and continue
                #different need to finish section and reset counter and set entry velocity
                if cRadius == nRadius:
                    sectionDistance += self.track[p]['lanes'][l]['length']
                    if self.track[p]['type'] == "STRAIGHT":
                        #print "banana"
                        #it is a straight so we should make sure that we start slow enough to brake in time for the next corner
                        thisSpeed = (sectionDistance / Tc) + nextSpeed
                    else:
                        #thisSpeed = math.sqrt(self.track[p]['lanes'][l]['radius'] * self.Fslip) + 1.0
                        thisSpeed = math.sqrt(self.track[p]['lanes'][l]['radius'] * self.Fslip) + self.track[p]['multiplier']
                else:
                    #new section
                    sectionDistance = self.track[p]['lanes'][l]['length']
                    nextType = self.track[q]['type']
                    #nextSpeed = math.sqrt(self.track[q]['lanes'][l]['radius'] * self.Fslip) + 1.0
                    nextSpeed = math.sqrt(self.track[q]['lanes'][l]['radius'] * self.Fslip) + self.track[q]['multiplier']
                    if self.track[p]['type'] == "STRAIGHT":
                        #print "apple"
                        #it is a straight so we should make sure that we start slow enough to brake in time for the next corner
                        thisSpeed = (sectionDistance / Tc) + nextSpeed
                    else:
                        #thisSpeed = math.sqrt(self.track[p]['lanes'][l]['radius'] * self.Fslip) + 1.0
                        thisSpeed = math.sqrt(self.track[p]['lanes'][l]['radius'] * self.Fslip) + self.track[p]['multiplier']

                # fix for no slip minimum
                if thisSpeed < math.sqrt(self.track[p]['lanes'][l]['radius'] * self.Fslip):
                    thisSpeed = math.sqrt(self.track[p]['lanes'][l]['radius'] * self.Fslip)
                if nextSpeed < math.sqrt(self.track[q]['lanes'][l]['radius'] * self.Fslip):
                    nextSpeed = math.sqrt(self.track[q]['lanes'][l]['radius'] * self.Fslip)

                self.track[p]['lanes'][l]['nextSectionIn'] = sectionDistance
                self.track[p]['lanes'][l]['nextType'] = nextType
                self.track[p]['lanes'][l]['nextSpeed'] = nextSpeed
                self.track[p]['lanes'][l]['thisSpeed'] = thisSpeed

        ###FIXME fix the first few also for the big corner to tight corner problem
            t = 1
            while self.track[-t]['lanes'][l]['radius'] == self.track[0]['lanes'][l]['radius']:
                self.track[-t]['lanes'][l]['nextSectionIn'] = self.track[-t]['lanes'][l]['nextSectionIn'] + self.track[0]['lanes'][l]['nextSectionIn']
                self.track[-t]['lanes'][l]['nextType'] = self.track[0]['lanes'][l]['nextType']
                self.track[-t]['lanes'][l]['nextSpeed'] = self.track[0]['lanes'][l]['nextSpeed']
                t = t+1

        nRadius = -1
        cRadius = self.track[-1]['lanes'][self.track[-1]['myLane']]['radius']
        nextType = self.track[0]['type']
        if nextType == 'STRAIGHT':
            nextSpeed = maxSpeed
        else:
            #nextSpeed = math.sqrt(self.track[0]['lanes'][self.track[0]['myLane']]['radius'] * self.Fslip) + 1.0
            nextSpeed = math.sqrt(self.track[0]['lanes'][self.track[0]['myLane']]['radius'] * self.Fslip) + self.track[0]['multiplier']
        sectionDistance = 0

        for p in reversed(range(len(self.track))):
            if p+1 == len(self.track):
                q = 0
            else:
                q = p+1
            #print json.dumps(p)
            #start simple with using track radius from 0 lane only
            nRadius = cRadius
            cRadius = self.track[p]['lanes'][self.track[p]['myLane']]['radius']
            # same so add some distance and continue
            #different need to finish section and reset counter and set entry velocity
            if cRadius == nRadius:
                sectionDistance += self.track[p]['lanes'][self.track[p]['myLane']]['length']
                #thisSpeed = math.sqrt(self.track[p]['lanes'][self.track[p]['myLane']]['radius'] * self.Fslip) + 1.0
                thisSpeed = math.sqrt(self.track[p]['lanes'][self.track[p]['myLane']]['radius'] * self.Fslip) + self.track[p]['multiplier']
                if self.track[p]['type'] == "STRAIGHT":
                    #print "banana"
                    #it is a straight so we should make sure that we start slow enough to brake in time for the next corner
                    thisSpeed = (sectionDistance / Tc) + nextSpeed
                else:
                    #thisSpeed = math.sqrt(self.track[p]['lanes'][self.track[p]['myLane']]['radius'] * self.Fslip) + 1.0
                    thisSpeed = math.sqrt(self.track[p]['lanes'][self.track[p]['myLane']]['radius'] * self.Fslip) + self.track[p]['multiplier']
            else:
                #new section
                sectionDistance = self.track[p]['lanes'][self.track[p]['myLane']]['length']
                nextType = self.track[q]['type']
                # the next speed should be the q tile this speed
                #nextSpeed = math.sqrt(self.track[q]['lanes'][self.track[q]['myLane']]['radius'] * self.Fslip) + 1.0
                if 'thisSpeed' in self.track[q]:
                    nextSpeed = self.track[q]['thisSpeed']
                else:
                    #nextSpeed = math.sqrt(self.track[q]['lanes'][self.track[q]['myLane']]['radius'] * self.Fslip) + 1.0
                    nextSpeed = math.sqrt(self.track[q]['lanes'][self.track[q]['myLane']]['radius'] * self.Fslip) + self.track[q]['multiplier']
                #thisSpeed = math.sqrt(self.track[p]['lanes'][self.track[p]['myLane']]['radius'] * self.Fslip) + 1.0
                thisSpeed = math.sqrt(self.track[p]['lanes'][self.track[p]['myLane']]['radius'] * self.Fslip) + self.track[p]['multiplier']
                if self.track[p]['type'] == "STRAIGHT":
                    #print "apple"
                    #it is a straight so we should make sure that we start slow enough to brake in time for the next corner
                    thisSpeed = (sectionDistance / Tc) + nextSpeed
                else:
                    #thisSpeed = math.sqrt(self.track[p]['lanes'][self.track[p]['myLane']]['radius'] * self.Fslip) + 1.0
                    thisSpeed = math.sqrt(self.track[p]['lanes'][self.track[p]['myLane']]['radius'] * self.Fslip) + self.track[p]['multiplier']

            # fix for no slip minimum
            if thisSpeed < math.sqrt(self.track[p]['lanes'][self.track[p]['myLane']]['radius'] * self.Fslip):
                thisSpeed = math.sqrt(self.track[p]['lanes'][self.track[p]['myLane']]['radius'] * self.Fslip)
            if nextSpeed < math.sqrt(self.track[q]['lanes'][self.track[q]['myLane']]['radius'] * self.Fslip):
                nextSpeed = math.sqrt(self.track[q]['lanes'][self.track[q]['myLane']]['radius'] * self.Fslip)

            self.track[p]['nextSectionIn'] = sectionDistance
            self.track[p]['nextType'] = nextType
            self.track[p]['nextSpeed'] = nextSpeed
            self.track[p]['thisSpeed'] = thisSpeed

        # fix the last tracks to line up with the first
        print "going to fix the next section distances"
        t = 1
        while self.track[-t]['lanes'][self.track[-t]['myLane']]['radius'] == self.track[0]['lanes'][self.track[0]['myLane']]['radius']:
            self.track[-t]['nextSectionIn'] = self.track[-t]['nextSectionIn'] + self.track[0]['nextSectionIn']
            self.track[-t]['nextType'] = self.track[0]['nextType']
            self.track[-t]['nextSpeed'] = self.track[0]['nextSpeed']
            # this is the boost on the last lap tile
            if self.track[-1]['type'] == 'STRAIGHT':
                self.turboTile = len(self.track)-t
            t = t+1

        print "TURBO TILE is ", self.turboTile


    def check_if_bumped(self):
        if self.timeConstant != 0:
            if (abs(self.cars[self.name]['now']['a']) > self.v1 and not self.turboState) or \
                ('turboFactor' in self.turboData and (abs(self.cars[self.name]['now']['a']) > self.v1*self.turboData['turboFactor'])):
                #bugger we got bumped
                self.bumpTick = self.gametick
                print "Stop bumping me. grrrr"


    def follow_lane_change(self):
        # this has a large part to do with following
        if self.following:
            if self.cars[self.whotofollow]['now']['tile'] != self.cars[self.whotofollow]['last']['tile'] and self.track[self.cars[self.whotofollow]['now']['tile']]['switch'] :
                #so the person we are following just hit a switch and we know if we need to change lane
                if self.cars[self.whotofollow]['now']['nextlane'] != self.cars[self.name]['now']['lane']:
                    # we need to change lane
                    if abs(self.cars[self.whotofollow]['now']['nextlane'] - self.cars[self.name]['now']['lane']) == 1:
                        # cool so we can change and stay behind
                        self.debugMessage += " switch to follow"
                        self.didLaneChange = True
                        # which way should we turn?? smaller lane is LEFT
                        if self.cars[self.whotofollow]['now']['nextlane'] - self.cars[self.name]['now']['lane'] < 0:
                            self.lane_change("Left")
                        else:
                            self.lane_change("Right")
                    else:
                        self.debugMessage += " couldn't make it to the same lane"
                        # need to turn of following and return to normal
                else:
                    self.debugMessage += " no change required"

    def update_slip_constants_old(self):
        """Calculate the track slip constants
        We require angular acceleration to be non-zero
        and previous to be zero

        Keyword arguments:
        """
        # ignore if already found
        if not self.magicAfound and self.get_alpha() != 0:
            vel = self.get_v(time='last')
            if self.track[self.cTile]['type'] == 'RIGHT':
                self.magicA = math.sqrt(self.get_radius()) * (abs(self.get_alpha()) + 0.3*vel) / (vel * vel)
            else:
                self.magicA = -1.0 * math.sqrt(self.get_radius()) * (abs(self.get_alpha()) - 0.3*vel) / (vel * vel)
            print "magic A:", self.magicA
            print "Fslip:", 0.09 / (self.magicA * self.magicA)
            self.magicAfound = True

    def check_turn_for_increase(self):
        if self.FslipFound:
            # do our processing
            if (self.turndirection == self.track[self.cars[self.name]['now']['tile']]['type']) or (self.track[self.cars[self.name]['now']['tile']]['type'] == 'STRAIGHT'):
                # we are in the same section just update max theta and turnfull throttle
                if abs(self.cars[self.name]['now']['t']) > self.turnMaxTheta:
                    self.turnMaxTheta = abs(self.cars[self.name]['now']['t'])
                # if a corner check that the speed is decent
                # if previous corner was tighter use that radius
                if self.turnFullThrottle:
                    rnow = self.track[self.cars[self.name]['now']['tile']]['lanes'][self.cars[self.name]['now']['lane']]['radius']
                    rprev = self.track[self.cars[self.name]['now']['tile']-1]['lanes'][self.cars[self.name]['now']['lane']]['radius']
                    if  rnow > rprev and self.track[self.cars[self.name]['now']['tile']-1] != 'STRAIGHT' and rnow < 180:
                        if self.track[self.cars[self.name]['now']['tile']]['type'] != 'STRAIGHT' and \
                            self.cars[self.name]['now']['v'] < math.sqrt(rprev * self.Fslip):
                            self.turnFullThrottle = False
                            print "can't increase because previous", self.cars[self.name]['now']['v']
                    #elif rnow >= 180:
                    #    # allow
                    #    print "greater than 180"
                    elif rnow < 180:
                        if self.track[self.cars[self.name]['now']['tile']]['type'] != 'STRAIGHT' and \
                            self.cars[self.name]['now']['v'] < math.sqrt(rnow * self.Fslip):
                            self.turnFullThrottle = False
                            print "can't increase because", self.cars[self.name]['now']['v']

            else:
                #new section
                if self.turnFullThrottle and self.turnMaxTheta < 40.0 and self.cars[self.name]['now']['lap'] > 0:
                    print "WE COULD GO FASTER FROM was less 40", self.turnStart
                    for t in range(self.turnStart, self.cars[self.name]['now']['tile']):
                        if self.track[t]['lanes'][self.cars[self.name]['now']['lane']]['radius'] > 50 or self.track[t]['lanes'][self.cars[self.name]['now']['lane']]['radius'] == 0:
                            print t, "from",self.track[t]['multiplier'],
                            self.track[t]['multiplier'] = self.track[t]['multiplier']+0.08
                            print "to",self.track[t]['multiplier']
                    self.update_corner_speeds()
                elif self.turnFullThrottle and self.turnMaxTheta < 50.0 and self.cars[self.name]['now']['lap'] > 0:
                    print "WE COULD GO FASTER FROM was less 50", self.turnStart
                    for t in range(self.turnStart, self.cars[self.name]['now']['tile']):
                        if self.track[t]['lanes'][self.cars[self.name]['now']['lane']]['radius'] > 50 or self.track[t]['lanes'][self.cars[self.name]['now']['lane']]['radius'] == 0:
                            print t, "from",self.track[t]['multiplier'],
                            self.track[t]['multiplier'] = self.track[t]['multiplier']+0.04
                            print "to",self.track[t]['multiplier']
                    self.update_corner_speeds()

                print "new turn direction at ", self.cars[self.name]['now']['tile'], self.cars[self.name]['now']['lap']
                self.turndirection = "" + self.track[self.cars[self.name]['now']['tile']]['type']
                self.turnMaxTheta = 0
                self.turnStart = self.cars[self.name]['now']['tile']
                self.turnFullThrottle = True


        elif self.timeConstant > 0:
            # first time through we need to get the first corner
            if self.turndirection == "" and self.track[self.cars[self.name]['now']['tile']]['type'] != 'STRAIGHT':
                self.turndirection = "" + self.track[self.cars[self.name]['now']['tile']]['type']
                self.turnMaxTheta = 0
                self.turnStart = self.cars[self.name]['now']['tile']
                self.turnFullThrottle = True

    def decrease_multiplier(self):
        t = self.cars[self.name]['now']['tile']
        if self.track[t]['type'] == 'STRAIGHT':
            direction = self.track[t-1]['type']
        else:
            direction = self.track[t]['type']
            t = self.cars[self.name]['now']['tile'] + 1
            #go forward also
            while direction == self.track[t]['type']:
                self.track[t]['multiplier'] = self.track[t]['multiplier']*0.90
                print "update tile ", t, "to", self.track[t]['multiplier']
                if t+1 >= len(self.track):
                    t = 0
                else:
                    t = t+1

        t = self.cars[self.name]['now']['tile']
        prevtype = "" + direction

        #go backward
        #while (direction == self.track[t]['type']) and (prevtype != 'STRAIGHT' and self.track[t]['type']):
        while (direction == self.track[t]['type']) or (prevtype != 'STRAIGHT' and self.track[t]['type'] == 'STRAIGHT'):
            self.track[t]['multiplier'] = self.track[t]['multiplier']*0.90
            prevtype = "" + self.track[t]['type']
            print "update tile ", t, "to", self.track[t]['multiplier']
            if t-1 < 0:
                t = len(self.track)-1
            else:
                t = t -1
        # need to run the update corners
        self.update_corner_speeds()

    def should_we_turbo(self, percent=65):
        """Good bloody question
        Will return true if this is a good location to turbo
        so if the percent of the turbo is reached without
        going over 60 percent

        Keyword arguments:
        percent -- number of ticks minimum with max throttle (default 75)
        """
        if self.get_turbo_available():
            # cool let's calculate
            dnow = {}
            dnow['r']=self.get_radius()
            dnow['x']=self.get_x()
            dnow['v']=self.get_v()
            dnow['a']=self.get_a()
            dnow['t']=self.get_theta()
            dnow['w']=self.get_w()
            dnow['alpha']=self.get_alpha()
            dnow['tile']=self.get_tile()
            dnow['startLaneIndex']=self.get_lane()
            dnow['endLaneIndex']=self.get_next_lane()
            dnow['tick']=self.gametick
            dnow['gas']=1.0

            percent = max(min(100,percent),1)
            #print percent,
            # number of ticks of turbo
            turboticks = int(percent * self.turboData['turboDurationTicks'] / 100)
            turboticks = max(min(self.turboData['turboDurationTicks'], turboticks),1)
            #print turboticks

            predicter = []
            predicter.append(dnow.copy())
            for x in range(0,200):
                if self.track[dnow['tile']]['switch'] and self.slipFound:
                    #if self.laneChanges[dnow['tile']-1] == 0:
                    #    dnow['endLaneIndex'] -= 1
                    #else:
                    #    dnow['endLaneIndex'] += 1

                    dnow['endLaneIndex'] = self.laneChanges[dnow['tile']-1]
                    # limit to lane range
                    #dnow['endLaneIndex'] = max(min(len(self.track[0]['lanes'])-1,dnow['endLaneIndex']),0)
                    #print "lane change prediction", dnow['startLaneIndex'], "to", dnow['endLaneIndex']

                if x <= turboticks:
                    dnow['gas']=1.0
                    dnow=self.predict_one_tick(dnow, turboForce=True)
                else:
                    dnow['gas']=0.0
                    dnow=self.predict_one_tick(dnow)
                #dnow=self.predict_one_tick(dnow,0.0)
        #        if self.gametick == 556:
        #            self.print_dnow(dnow)
                predicter.append(dnow.copy())
                if abs(dnow['t']) > 60.0:
                    #print predicter
                    #if self.gametick == 371:
        #            print "#############################################", self.braketick
                    #break
                    return False
            else:
                print "we should turbo"
                return True
                #for p in predicter:
                #    self.print_dnow(p)
                #print "--------------------------------"

        else:
            # Turbo not available
            return False



    """
                                                                                     d8b 888    d8b
                                                                                     Y8P 888    Y8P
                                                                                         888
     .d88b.  88888b.        .d8888b  8888b.  888d888      88888b.   .d88b.  .d8888b  888 888888 888  .d88b.  88888b.  .d8888b
    d88""88b 888 "88b      d88P"        "88b 888P"        888 "88b d88""88b 88K      888 888    888 d88""88b 888 "88b 88K
    888  888 888  888      888      .d888888 888          888  888 888  888 "Y8888b. 888 888    888 888  888 888  888 "Y8888b.
    Y88..88P 888  888      Y88b.    888  888 888          888 d88P Y88..88P      X88 888 Y88b.  888 Y88..88P 888  888      X88
     "Y88P"  888  888       "Y8888P "Y888888 888          88888P"   "Y88P"   88888P' 888  "Y888 888  "Y88P"  888  888  88888P'
                                                          888
                                                          888
                                                          888
    """
    def on_car_positions(self, data):
        """Give throttle of lane change command based on data
        Process data and provide the appropriate command 

        Keyword arguments:
        carname -- the car name (default self.name)
        tile -- the tile (default current tile)
        """
        # previous throttle
        self.theLastGas = self.theGas

        # clean positions
        self.positions = {}
        # motion data for all race cars
        # required for future calculations
        for car in data:
            self.update_racer_data(car)


        for racer in data:
            if racer['id']['name'] == self.name:
                #update the motion data
                self.update_motion_data(racer)

                # this will only run during the first few ticks to calculate the top speed, time constant etc.
                self.update_performance_constants()

                # will only run while we don't have the magic A
                self.update_slip_constants()
                #self.update_all_slip_constants()

                # check for dodging
                #self.best_is_car_ahead()

                doTurbo = False

                if self.gametick >= 1:
                    dnow = {}
                    dnow['r']=self.get_radius()
                    dnow['x']=self.get_x()
                    dnow['v']=self.get_v()
                    dnow['a']=self.get_a()
                    dnow['t']=self.get_theta()
                    dnow['w']=self.get_w()
                    dnow['alpha']=self.get_alpha()
                    dnow['tile']=self.get_tile()
                    dnow['startLaneIndex']=self.get_lane()
                    dnow['endLaneIndex']=self.get_next_lane()
                    dnow['tick']=self.gametick
                    dnow['gas']=self.theGas

                    predicter = []
                    predicter.append(dnow.copy())
                    self.theGas = 0.0
                    if self.gametick >= self.braketick:
                        for x in range(0,200):
                            if self.track[dnow['tile']]['switch'] and self.slipFound:
                                dnow['endLaneIndex'] = self.laneChanges[dnow['tile']-1]
                            if x == 0:
                                dnow['gas']=1.0
                                dnow=self.predict_one_tick(dnow)
                            else:
                                dnow['gas']=0.0
                                dnow=self.predict_one_tick(dnow)
                            predicter.append(dnow.copy())
                            if abs(dnow['t']) > 60.0:
                                self.braketick = dnow['tick']
                                self.theGas = 0.0
                                for p in predicter:
                                    self.print_dnow(p, message='ON CAR')
                                break
                        else:
                            self.theGas = 1.0
                        #print "testskills"
                            # turbo find
                            doTurbo = self.should_we_turbo()
                            if doTurbo:
                                print "Not even scared...BOOOST!",


        # gas all the way to the end if STRAIGHT
        if self.get_tile() >= self.homeStraightTile and self.get_lap() == self.totalLaps - 1:
            self.theGas = 1.0
            self.turboStraight = self.get_tile()

        if (self.name[:6] == 'puzzle') and (not self.slipFound):
            self.theGas = 1.0

        ############## Predicted velocity ###################
        self.cars[self.name]['now']['pv'] = float(self.predict_velocity())

        self.print_table()

        if self.get_tile() != self.get_tile(time='last') and self.get_tile() in self.laneChanges and self.slipFound:
            if 'LANE' in self.commandStack:
                print "already lane request", self.commandStack,
            else:
                self.commandStack.append('LANE')

        #if self.cars[self.name]['turboAvailable'] and self.get_tile() != self.get_tile(time='last') and self.get_tile() == self.turboStraight:
        if (self.cars[self.name]['turboAvailable'] and self.get_tile() == self.turboStraight) or doTurbo:
            # turbo available, on the correct tile
            if 'TURBO' in self.commandStack:
                print "already turbo request", self.commandStack,
            else:
                self.commandStack.append('TURBO')

        if self.theGas != self.theLastGas:
            self.commandStack.append('GAS')



        ############## COMMAND TO SEND #######################
        if len(self.commandStack) == 0:
            # no pressing commands so just send gas
            print "GAS", self.gametick
            self.throttle(self.theGas)
        else:
            cmd = self.commandStack.pop()
            if cmd == 'GAS':
                print "GAS CHANGE", self.gametick
                self.throttle(self.theGas)
            elif cmd == 'LANE':
                print "LANE CHANGE",
                if self.laneChanges[self.get_tile()] == 0:
                    print "LEFT", self.gametick
                    self.lane_change("Left")
                else:
                    print "RIGHT", self.gametick
                    self.lane_change("Right")
            elif cmd == 'TURBO':
                print "TURBO PLEASE", self.gametick
                self.set_turbo()
            else:
                print "unknown command", cmd, self.gametick
                self.throttle(self.theGas)




    """
                           888          888                          888 d8b                                                  888                      888
                           888          888                          888 Y8P                                                  888                      888
                           888          888                          888                                                      888                      888
    888  888 88888b.   .d88888  8888b.  888888 .d88b.       .d8888b  888 888 88888b.        .d8888b .d88b.  88888b.  .d8888b  888888  8888b.  88888b.  888888 .d8888b
    888  888 888 "88b d88" 888     "88b 888   d8P  Y8b      88K      888 888 888 "88b      d88P"   d88""88b 888 "88b 88K      888        "88b 888 "88b 888    88K
    888  888 888  888 888  888 .d888888 888   88888888      "Y8888b. 888 888 888  888      888     888  888 888  888 "Y8888b. 888    .d888888 888  888 888    "Y8888b.
    Y88b 888 888 d88P Y88b 888 888  888 Y88b. Y8b.               X88 888 888 888 d88P      Y88b.   Y88..88P 888  888      X88 Y88b.  888  888 888  888 Y88b.       X88
     "Y88888 88888P"   "Y88888 "Y888888  "Y888 "Y8888        88888P' 888 888 88888P"        "Y8888P "Y88P"  888  888  88888P'  "Y888 "Y888888 888  888  "Y888  88888P'
             888                                                             888
             888                                                             888
             888                                                             888
    """
    def update_slip_constants(self):
        """Calculate the track slip constants
        We require:
        -theta and w and alpha to be non zero.
        -car to be accelerating or braking
        -all values to be on the same radius and direction corner

        Once calculated we continue immediately.

        Keyword arguments:
        """
        if not self.slipFound:
            #clean on tile change
            if self.get_tile() != self.get_tile(time='last') and \
                    (self.get_radius() != self.get_radius(time='last') or \
                    self.track[self.get_tile()]['type'] != self.track[self.get_tile(time='last')]['type']):
                print "reset", self.gametick
                self.atemp = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
                self.btemp = [0,0,0,0]
                self.rtemp = [0,0,0,0]
                self.slipCheckTick = self.gametick
            # clean on bad bump
            if abs(self.get_v() - self.get_pv(time='last')) > 0.000001:
                print "bump reset", self.gametick
                self.atemp = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
                self.btemp = [0,0,0,0]
                self.rtemp = [0,0,0,0]
                self.slipCheckTick = self.gametick

            if self.track[self.get_tile()]['type'] != 'STRAIGHT' and self.gametick > self.slipCheckTick:
                # so we need to calculate a slip for this corner

                # if abs(theta last) > 0
                    #push and
                    #add values
                # if all values are equal radius and direction and all are full
                    #process and add to dict

                if abs(self.get_theta(time='last')) > 0:
                    print "update", self.gametick
                    self.atemp[3] = list(self.atemp[2])
                    self.atemp[2] = list(self.atemp[1])
                    self.atemp[1] = list(self.atemp[0])

                    self.btemp[3] = self.btemp[2]
                    self.btemp[2] = self.btemp[1]
                    self.btemp[1] = self.btemp[0]
                    self.btemp[0] = self.get_alpha()

                    self.rtemp[3] = self.rtemp[2]
                    self.rtemp[2] = self.rtemp[1]
                    self.rtemp[1] = self.rtemp[0]
                    if self.track[self.get_tile()]['type'] == 'RIGHT':
                        self.rtemp[0] = self.get_radius()
                        self.atemp[0][0] = (self.get_v(time='last') * self.get_v(time='last'))/math.sqrt(self.get_radius())
                        self.atemp[0][1] = -1.0 * self.get_v(time='last')
                    else:
                        self.rtemp[0] = -1.0 * self.get_radius()
                        self.atemp[0][0] = (-1.0 * self.get_v(time='last') * self.get_v(time='last'))/math.sqrt(self.get_radius())
                        self.atemp[0][1] = self.get_v(time='last')
                    self.atemp[0][2] = -1.0 * self.get_w(time='last')
                    self.atemp[0][3] = -1.0 * self.get_theta(time='last') * self.get_v(time='last')



                if abs(self.btemp[3]) > 0 and abs(self.btemp[2]) > 0 and abs(self.btemp[1]) > 0 and abs(self.btemp[0]) > 0 and \
                self.rtemp[3] == self.rtemp[2] and self.rtemp[2] == self.rtemp[1] and self.rtemp[1] == self.rtemp[0]:
                    # now we are ready
                    print "using these to calculate for ", self.get_tile()
                    print self.atemp[0]
                    print self.atemp[1]
                    print self.atemp[2]
                    print self.atemp[3]
                    print self.btemp
                    print self.rtemp

                    a = np.array(self.atemp)
                    b = np.array(self.btemp)
                    checkPass = False
                    try:
                        x = np.linalg.solve(a, b)
                        print "possible solution:", x,
                        checkPass = np.allclose(np.dot(a, x), b)
                        print checkPass
                    except Exception:
                        checkPass = False
                    #else:
                    #    checkPass = True

                    print "check:",
                    if checkPass:
                        print "PASSED, paddle found"
                        if abs(x[2]) > 2.0 or abs(x[3]) > 2.0 or abs(x[0]) > 2.0 or abs(x[1]) > 2.0 or (x[1]*x[1] / (x[0]*x[0])) > 2.0 or \
                        abs(x[0]) < 0.0000001 or abs(x[1]) < 0.0000001 or (x[1]*x[1] / (x[0]*x[0])) < 0.1 or \
                        x[0] < 0.0 or x[1] < 0.0 or x[2] < 0.0 or x[3] < 0.0:
                            # these are bollocks
                            print "FAIL: BOLLOCKS CALCULATIONS as the speed was too slow"
                            # use defaults
                            self.W = 0.1
                            self.T = 0.00125
                            self.A = 0.53033008589
                            self.B = 0.3
                            self.Fslip = 0.35
                        else:
                            self.W = x[2]
                            self.T = x[3]
                            self.A = x[0]
                            self.B = x[1]
                            self.Fslip = self.B*self.B / (self.A * self.A)
                            self.slipFound = True
                    else:
                        print "FAIL: you are up the creek without a paddle"
                        # use defaults
                        self.W = 0.1
                        self.T = 0.00125
                        self.A = 0.53033008589
                        self.B = 0.3
                        self.Fslip = 0.35
                    print "W:",self.W
                    print "T:", self.T
                    print "A:", self.A
                    print "B:", self.B
                    print "Fslip:", self.Fslip
                    #clean the others ready
                    self.atemp = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
                    self.btemp = [0,0,0,0]
                    self.rtemp = [0,0,0,0]


    def update_slip_constants_OLD(self):
        """Calculate the track slip constants
        We require:
        -theta and w and alpha to be non zero.
        -car to be accelerating or braking
        -all values to be on the same radius and direction corner

        Once calculated we continue immediately.

        Keyword arguments:
        """
        if not self.slipFound:
            # let's use the first few ticks and get some solid constants for our transfer function
            if self.b[3] == 0 and abs(self.b[2]) > 0:
                self.b[3] = self.get_alpha()
                if self.track[self.get_tile()]['type'] == 'RIGHT':
                    self.step4[0] = (self.get_v(time='last') * self.get_v(time='last'))/math.sqrt(self.get_radius())
                    self.step4[1] = -1.0 * self.get_v(time='last')
                else:
                    self.step4[0] = (-1.0 * self.get_v(time='last') * self.get_v(time='last'))/math.sqrt(self.get_radius())
                    self.step4[1] = self.get_v(time='last')
                self.step4[2] = -1.0 * self.get_w(time='last')
                self.step4[3] = -1.0 * self.get_theta(time='last') * self.get_v(time='last')

            if self.b[2] == 0 and abs(self.b[1]) > 0:
                self.b[2] = self.get_alpha()
                if self.track[self.get_tile()]['type'] == 'RIGHT':
                    self.step3[0] = (self.get_v(time='last') * self.get_v(time='last'))/math.sqrt(self.get_radius())
                    self.step3[1] = -1.0 * self.get_v(time='last')
                else:
                    self.step3[0] = (-1.0 * self.get_v(time='last') * self.get_v(time='last'))/math.sqrt(self.get_radius())
                    self.step3[1] = self.get_v(time='last')
                self.step3[2] = -1.0 * self.get_w(time='last')
                self.step3[3] = -1.0 * self.get_theta(time='last') * self.get_v(time='last')

            if self.b[1] == 0 and abs(self.b[0]) > 0:
                self.b[1] = self.get_alpha()
                if self.track[self.get_tile()]['type'] == 'RIGHT':
                    self.step2[0] = (self.get_v(time='last') * self.get_v(time='last'))/math.sqrt(self.get_radius())
                    self.step2[1] = -1.0 * self.get_v(time='last')
                else:
                    self.step2[0] = (-1.0 * self.get_v(time='last') * self.get_v(time='last'))/math.sqrt(self.get_radius())
                    self.step2[1] = self.get_v(time='last')
                self.step2[2] = -1.0 * self.get_w(time='last')
                self.step2[3] = -1.0 * self.get_theta(time='last') * self.get_v(time='last')

            if self.b[0] == 0 and abs(self.get_theta(time='last')) > 0:
                print "try to solve the system of linear equations"
                self.b[0] = self.get_alpha()
                if self.track[self.get_tile()]['type'] == 'RIGHT':
                    self.step1[0] = (self.get_v(time='last') * self.get_v(time='last'))/math.sqrt(self.get_radius())
                    self.step1[1] = -1.0 * self.get_v(time='last')
                else:
                    self.step1[0] = (-1.0 * self.get_v(time='last') * self.get_v(time='last'))/math.sqrt(self.get_radius())
                    self.step1[1] = self.get_v(time='last')
                self.step1[2] = -1.0 * self.get_w(time='last')
                self.step1[3] = -1.0 * self.get_theta(time='last') * self.get_v(time='last')

            if abs(self.b[3]) > 0:
                # now we are ready
                print "using these to calculate..."
                print self.step1
                print self.step2
                print self.step3
                print self.step4
                print self.b

                a = np.array([self.step1, self.step2, self.step3, self.step4])
                b = np.array(self.b)
                x = np.linalg.solve(a, b)
                print "possible solution:", x
                print "check:",
                if np.allclose(np.dot(a, x), b):
                    print "PASSED"
                    self.W = x[2]
                    self.T = x[3]
                    self.A = x[0]
                    self.B = x[1]
                    self.slipFound = True
                else:
                    print "FAIL: you are up the creek without a paddle"
                    # use defaults
                    self.W = 0.1
                    self.T = 0.00125
                    self.A = 0.53033008589
                    self.B = 0.3
                    self.Fslip = self.B*self.B / (self.A * self.A)
                    self.slipFound = True
                print "W:",self.W
                print "T:", self.T
                print "A:", self.A
                print "B:", self.B
                print "Fslip:", self.B*self.B / (self.A * self.A)

    def update_all_slip_constants(self):
        """Calculate the track slip constants
        We require:
        -theta and w and alpha to be non zero.
        -car to be accelerating or braking
        -all values to be on the same radius and direction corner

        Once calculated we continue immediately.

        Keyword arguments:
        """

        #clean on tile change
        if self.get_tile() != self.get_tile(time='last') and \
                (self.get_radius() != self.get_radius(time='last') or \
                self.track[self.get_tile()]['type'] != self.track[self.get_tile(time='last')]['type']):
            print "reset", self.gametick
            self.atempall = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
            self.btempall = [0,0,0,0]
            self.rtempall = [0,0,0,0]
            self.slipCheckTickall = self.gametick


        if self.get_tile() not in self.slips and self.track[self.get_tile()]['type'] != 'STRAIGHT' and self.gametick > self.slipCheckTickall:
            # so we need to calculate a slip for this corner

            # if abs(theta last) > 0
                #push and
                #add values
            # if all values are equal radius and direction and all are full
                #process and add to dict

            if abs(self.get_theta(time='last')) > 0:
                self.atempall[3] = list(self.atempall[2])
                self.atempall[2] = list(self.atempall[1])
                self.atempall[1] = list(self.atempall[0])

                self.btempall[3] = self.btempall[2]
                self.btempall[2] = self.btempall[1]
                self.btempall[1] = self.btempall[0]
                self.btempall[0] = self.get_alpha()

                self.rtempall[3] = self.rtempall[2]
                self.rtempall[2] = self.rtempall[1]
                self.rtempall[1] = self.rtempall[0]
                if self.track[self.get_tile()]['type'] == 'RIGHT':
                    self.rtempall[0] = self.get_radius()
                    self.atempall[0][0] = (self.get_v(time='last') * self.get_v(time='last'))/math.sqrt(self.get_radius())
                    self.atempall[0][1] = -1.0 * self.get_v(time='last')
                else:
                    self.rtempall[0] = -1.0 * self.get_radius()
                    self.atempall[0][0] = (-1.0 * self.get_v(time='last') * self.get_v(time='last'))/math.sqrt(self.get_radius())
                    self.atempall[0][1] = self.get_v(time='last')
                self.atempall[0][2] = -1.0 * self.get_w(time='last')
                self.atempall[0][3] = -1.0 * self.get_theta(time='last') * self.get_v(time='last')



            if abs(self.btempall[3]) > 0 and abs(self.btempall[2]) > 0 and abs(self.btempall[1]) > 0 and abs(self.btempall[0]) > 0 and \
            self.rtempall[3] == self.rtempall[2] and self.rtempall[2] == self.rtempall[1] and self.rtempall[1] == self.rtempall[0]:
                # now we are ready
                print "ALL using these to calculate for ", self.get_tile()
                print self.atempall[0]
                print self.atempall[1]
                print self.atempall[2]
                print self.atempall[3]
                print self.btempall
                print self.rtempall


                a = np.array(self.atempall)
                b = np.array(self.btempall)
                checkPass = False
                try:
                    x = np.linalg.solve(a, b)
                    print "ALL possible solution:", x,
                    checkPass = np.allclose(np.dot(a, x), b)
                    print checkPass
                except Exception:
                    checkPass = False

                print "ALL check:",
                if checkPass:
                    print "PASSED"
                    W = x[2]
                    T = x[3]
                    A = x[0]
                    B = x[1]
                    # make sure these are valid
                    if abs(W) > 2.0 or abs(T) > 2.0 or abs(A) > 2.0 or abs(B) > 2.0 or (B*B / (A*A)) > 2.0 or \
                    abs(A) < 0.0000001 or abs(B) < 0.0000001:
                        # these are bollocks
                        print "FAIL: BOLLOCKS CALCULATIONS as the speed was too slow"
                    else:
                        self.slips[self.get_tile()] = [W,T,A,B]
                    print "W:",W
                    print "T:", T
                    print "A:", A
                    print "B:", B
                    print "Fslip:", B*B / (A*A)
                    print json.dumps(self.slips)
                else:
                    print "FAIL: you are up the creek without a paddle"
                    # use defaults
                    #W = 0.1
                    #T = 0.00125
                    #A = 0.53033008589
                    #B = 0.3
                
                #clean the others ready
                self.atempall = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
                self.btempall = [0,0,0,0]
                self.rtempall = [0,0,0,0]

    """
                           888          888                                                                                   888                      888
                           888          888                                                                                   888                      888
                           888          888                                                                                   888                      888
    888  888 88888b.   .d88888  8888b.  888888 .d88b.        .d8888b  8888b.  888d888       .d8888b .d88b.  88888b.  .d8888b  888888  8888b.  88888b.  888888 .d8888b
    888  888 888 "88b d88" 888     "88b 888   d8P  Y8b      d88P"        "88b 888P"        d88P"   d88""88b 888 "88b 88K      888        "88b 888 "88b 888    88K
    888  888 888  888 888  888 .d888888 888   88888888      888      .d888888 888          888     888  888 888  888 "Y8888b. 888    .d888888 888  888 888    "Y8888b.
    Y88b 888 888 d88P Y88b 888 888  888 Y88b. Y8b.          Y88b.    888  888 888          Y88b.   Y88..88P 888  888      X88 Y88b.  888  888 888  888 Y88b.       X88
     "Y88888 88888P"   "Y88888 "Y888888  "Y888 "Y8888        "Y8888P "Y888888 888           "Y8888P "Y88P"  888  888  88888P'  "Y888 "Y888888 888  888  "Y888  88888P'
             888
             888
             888
    """
    def update_performance_constants(self):
        """Calculate the car performance constants
        We require three steps of time and constant
        throttle throughout the calculation. 
        So give it the gas 1.0

        Keyword arguments:
        """
        # let's use the first few ticks and get some solid constants for our transfer function
        if self.k == 0 and self.mass == 0:
            # first lets find the 3 speeds we need
            if self.v2 == 0 and self.v1 > 0:
                self.v2 = self.get_v()
                self.k2 = 1 - (self.a / self.k1)
            if self.v1 == 0 and self.get_v() > 0:
                self.v1 = self.get_v()
                self.k1 = self.get_v()
            # do we have v1, v2 and v3 also assume throttle was 1.0
            if self.v1 > 0 and self.v2 > 0:
                self.k = ( self.v1 + self.v1 - self.v2 ) / ( self.v1 * self.v1 * 1.0 )
                print "k:", self.k
                print "k1:", self.k1
                print "k2:", self.k2
                self.maxSpeed = 1.0 / self.k
                print "top speed:", self.maxSpeed
                self.mass = ( -1.0 * self.k ) / math.log((self.v2 - self.maxSpeed) / (self.v1 - self.maxSpeed))
                print "mass:", self.mass
                self.timeConstant = self.mass / self.k
                print "time constant:", self.timeConstant

    """
                     d8b          888         888             888      888
                     Y8P          888         888             888      888
                                  888         888             888      888
    88888b.  888d888 888 88888b.  888888      888888  8888b.  88888b.  888  .d88b.
    888 "88b 888P"   888 888 "88b 888         888        "88b 888 "88b 888 d8P  Y8b
    888  888 888     888 888  888 888         888    .d888888 888  888 888 88888888
    888 d88P 888     888 888  888 Y88b.       Y88b.  888  888 888 d88P 888 Y8b.
    88888P"  888     888 888  888  "Y888       "Y888 "Y888888 88888P"  888  "Y8888
    888
    888
    888
    """

    def print_table(self):
        # angle, pieceIndex, pieceindistane, lane
        #print json.dumps(racer)
        if self.DEBUG:
            if self.gametick == 1:
                print "x,tick,gas,v,a, ,theta,w,alpha, ,tile,message"
        #if self.DEBUG or self.gametick < 150:
            #print('{0:4d} {1:{width}f} {2:{width}f} {3:{width}f} {4:2d} {5:{width}f} {6: d} {7: d}'.format( #read
            #print('{0:d},{1:f},{2:f},{3:f},{4:f},{5:f},{6:f},{7:f},{8:d},{9:d},{10:d},{11:d},{12:f}'.format( #data
            if self.letsBumpem:
                print('{:.6f},{:d},{:.3f},{:.5f},{:.3f},p{:.5f},p{:.5f},{},{:.3f},{:.3f},{:.3f},{},p{:.5f},p{:.5f},p{:.5f},{},{:d},{:d},L{:d},{},{},{:.3f}'.format( #data
                    self.cPos,
                    self.gametick,
                    self.theGas,
                    #self.cLapDistance,
                    self.get_v(),
                    self.get_a(),
                    self.get_pv(),
                    self.predict_velocity(),
                    " ",
                    self.get_theta(),
                    self.get_w(),
                    self.get_alpha(),
                    " ",
                    self.predict_variable(variable='t'),
                    self.predict_variable(variable='w'),
                    self.predict_variable(variable='alpha'),
                    " ",
                    #racer['piecePosition']['pieceIndex'], 
                    #racer['piecePosition']['lane']['startLaneIndex'],
                    #self.track[self.cTile]['myLane'],
                    #self.cLane,
                    #self.track[self.cTile]['lanes'][self.track[self.cTile]['myLane']]['radius'],
                    self.get_radius(),
                    self.cTile,
                    self.get_lane(),
                    self.debugMessage,
                    "BUMPEM " + self.whotofollow,
                    self.aheadx,
                    width=6)),
            else:
                #print('{0:f},{1:f},{2:f},{3:f},{4:f},{5:f},{6:f},{7:f},{8:d}'.format( #data
                print('{:.6f},{:d},{:.3f},{:.5f},{:.5f},p{:.5f},p{:.5f},{},{:.5f},{:.5f},{:.5f},{},p{:.5f},p{:.5f},p{:.5f},{},{:d},{:d},L{:d},{}'.format( #data
                    self.cPos,
                    self.gametick,
                    self.theGas,
                    #self.cLapDistance,
                    self.get_v(),
                    self.get_a(),
                    self.get_pv(),
                    self.predict_velocity(),
                    " ",
                    self.get_theta(),
                    self.get_w(),
                    self.get_alpha(),
                    " ",
                    self.predict_variable(variable='t'),
                    self.predict_variable(variable='w'),
                    self.predict_variable(variable='alpha'),
                    " ",
                    #racer['piecePosition']['pieceIndex'], 
                    #racer['piecePosition']['lane']['startLaneIndex'],
                    #self.track[self.cTile]['myLane'],
                    #self.cLane,
                    #self.track[self.cTile]['lanes'][self.track[self.cTile]['myLane']]['radius'],
                    self.get_radius(),
                    #self.get_radius(startLaneIndex=1),
                    #self.get_radius(startLaneIndex=2),
                    self.cTile,
                    self.get_lane(),
                    self.debugMessage,
                    width=6)),
                ### DATALOG
                #for p in range(self.totalPieces):
                #    print self.cars[self.name]['nowEntrySpeeds'][p],
                #print ""

    def print_dnow(self, d, message=''):
        # angle, pieceIndex, pieceindistane, lane
        #print json.dumps(racer)
        if self.DEBUG:
            #print('{0:f},{1:f},{2:f},{3:f},{4:f},{5:f},{6:f},{7:f},{8:d}'.format( #data
            print('{:.6f},{:d},{:.3f},{:.5f},{:.5f},{},{:.5f},{:.5f},{:.5f},{},{:d},{:d},S{:d},E{:d},{},{}'.format( #data
                d['x'],
                d['tick'],
                d['gas'],
                #self.cLapDistance,
                d['v'],
                d['a'],
                #self.predict_velocity(),
                " ",
                d['t'],
                d['w'],
                d['alpha'],
                #self.update_angles(),
                " ",
                #racer['piecePosition']['pieceIndex'], 
                #racer['piecePosition']['lane']['startLaneIndex'],
                #self.track[self.cTile]['myLane'],
                #self.cLane,
                #self.track[self.cTile]['lanes'][self.track[self.cTile]['myLane']]['radius'],
                #self.get_radius(startLaneIndex=0),
                #self.get_radius(startLaneIndex=1),
                #self.get_radius(startLaneIndex=2),
                d['r'],
                d['tile'],
                d['startLaneIndex'],
                d['endLaneIndex'],
                "PREDICTION",
                message,
                width=6))


    """
                           888                     888
                           888                     888
                           888                     888
     .d88b.  88888b.       888888 888  888 888d888 88888b.   .d88b.
    d88""88b 888 "88b      888    888  888 888P"   888 "88b d88""88b
    888  888 888  888      888    888  888 888     888  888 888  888
    Y88..88P 888  888      Y88b.  Y88b 888 888     888 d88P Y88..88P
     "Y88P"  888  888       "Y888  "Y88888 888     88888P"   "Y88P"
    """
    def on_turbo(self, data):
        print "Turbo Available",
        self.turboData = data
        for n, car in self.cars.items():
            if car['racing']:
                car['turboAvailable'] = True
        if self.cars[self.name]['racing']:
            print "... for me too",
        print ""

    def on_turbo_start(self, data):
        self.cars[data['name']]['turboing'] = True
        self.cars[data['name']]['turboAvailable'] = False
        self.cars[data['name']]['turboStartTick'] = self.gametick
        self.cars[data['name']]['turboEndTick'] = self.gametick + self.turboData['turboDurationTicks']
        if data['name'] == self.name:
            print "TURBO ON at", self.gametick

    def on_turbo_end(self, data):
        self.cars[data['name']]['turboing'] = False
        if data['name'] == self.name:
            print "TURBO END :( at", self.gametick

    """
                                                             888
                                                             888
                                                             888
     .d88b.  88888b.        .d8888b 888d888 8888b.  .d8888b  88888b.
    d88""88b 888 "88b      d88P"    888P"      "88b 88K      888 "88b
    888  888 888  888      888      888    .d888888 "Y8888b. 888  888
    Y88..88P 888  888      Y88b.    888    888  888      X88 888  888
     "Y88P"  888  888       "Y8888P 888    "Y888888  88888P' 888  888
     """
    def on_crash(self, data):
        self.cars[data['name']]['turboing'] = False
        self.cars[data['name']]['turboEndTick'] = self.gametick - 1



        print "Someone crashed"
        if data['name'] == self.name:
            print "It was you who crashed"
            self.racing = False
            self.ping()
        if data['name'] == self.whotofollow:
            print "the bloke we were following crashed....the idiot"
            #we don't want to follow this idiot anymore he crashed
            self.following = False
            self.whotofollow = ""
        self.cars[data['name']]['racing'] = False
        #self.ping()


    """
     .d88b.  88888b.       .d8888b  88888b.   8888b.  888  888  888 88888b.
    d88""88b 888 "88b      88K      888 "88b     "88b 888  888  888 888 "88b
    888  888 888  888      "Y8888b. 888  888 .d888888 888  888  888 888  888
    Y88..88P 888  888           X88 888 d88P 888  888 Y88b 888 d88P 888  888
     "Y88P"  888  888       88888P' 88888P"  "Y888888  "Y8888888P"  888  888
                                    888
                                    888
                                    888
    """
    def on_spawn(self, data):
        print("Someone was spawned")
        if data['name'] == self.name:
            print("you")
            self.racing = True
            self.theGas = 1.0
            self.throttle(self.theGas)
        self.cars[data['name']]['racing'] = True
        #self.ping()



    """
                           888                         .d888 d8b          d8b          888                    888
                           888                        d88P"  Y8P          Y8P          888                    888
                           888                        888                              888                    888
     .d88b.  88888b.       888  8888b.  88888b.       888888 888 88888b.  888 .d8888b  88888b.   .d88b.   .d88888
    d88""88b 888 "88b      888     "88b 888 "88b      888    888 888 "88b 888 88K      888 "88b d8P  Y8b d88" 888
    888  888 888  888      888 .d888888 888  888      888    888 888  888 888 "Y8888b. 888  888 88888888 888  888
    Y88..88P 888  888      888 888  888 888 d88P      888    888 888  888 888      X88 888  888 Y8b.     Y88b 888
     "Y88P"  888  888      888 "Y888888 88888P"       888    888 888  888 888  88888P' 888  888  "Y8888   "Y88888
                                        888
                                        888
                                        888
    """
    def on_lap_finished(self, data):
        print"Lap", data['lapTime']['lap'], "finished:", data['car']['name'], data['lapTime']['millis'], "ms  Ranking:", data['ranking']['overall']
        if data['car']['name'] == self.name and self.fastestLap > data['lapTime']['millis']:
            self.fastestLap = data['lapTime']['millis']
        #self.ping()



    """
                            .d888 d8b          d8b          888
                           d88P"  Y8P          Y8P          888
                           888                              888
     .d88b.  88888b.       888888 888 88888b.  888 .d8888b  88888b.
    d88""88b 888 "88b      888    888 888 "88b 888 88K      888 "88b
    888  888 888  888      888    888 888  888 888 "Y8888b. 888  888
    Y88..88P 888  888      888    888 888  888 888      X88 888  888
     "Y88P"  888  888      888    888 888  888 888  88888P' 888  888
    """
    def on_finish(self, data):
        print"Finish:", data['name']
        if data['name'] == self.name:
            print "Finish by me"
        #self.ping()


    """
                                                                                                888
                                                                                                888
                                                                                                888
     .d88b.  88888b.        .d88b.   8888b.  88888b.d88b.   .d88b.        .d88b.  88888b.   .d88888
    d88""88b 888 "88b      d88P"88b     "88b 888 "888 "88b d8P  Y8b      d8P  Y8b 888 "88b d88" 888
    888  888 888  888      888  888 .d888888 888  888  888 88888888      88888888 888  888 888  888
    Y88..88P 888  888      Y88b 888 888  888 888  888  888 Y8b.          Y8b.     888  888 Y88b 888
     "Y88P"  888  888       "Y88888 "Y888888 888  888  888  "Y8888        "Y8888  888  888  "Y88888
                                888
                           Y8b d88P
                            "Y88P"
    """
    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_dnf(self, data):
        print("DNF: {0}".format(data))
        #self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'joinRace': self.on_join_race,
            'createRace': self.on_create_race,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'turboAvailable': self.on_turbo,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'lapFinished': self.on_lap_finished,
            'finish': self.on_finish,
            'gameEnd': self.on_game_end,
            'dnf': self.on_dnf,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.gametick = msg['gameTick']
                #print "found gametick", self.gametick
            #else:
            #    print "gametick is", self.gametick
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                print json.dumps(data)
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = SpeedRacer(s, name, key)
        bot.run()
